# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table article (
  id                        integer auto_increment not null,
  title                     TEXT,
  summary                   TEXT,
  author                    TEXT,
  date                      datetime,
  body                      TEXT,
  source                    TEXT,
  category                  TEXT,
  url                       TEXT,
  indexed                   tinyint(1) default 0,
  image                     varchar(255),
  constraint pk_article primary key (id))
;

create table favorite (
  id                        integer auto_increment not null,
  user_user_id              integer not null,
  constraint pk_favorite primary key (id))
;

create table user (
  user_id                   integer auto_increment not null,
  facebookid                TEXT,
  name                      TEXT,
  birthday                  datetime,
  email                     TEXT,
  password                  TEXT,
  constraint pk_user primary key (user_id))
;

alter table favorite add constraint fk_favorite_user_1 foreign key (user_user_id) references user (user_id) on delete restrict on update restrict;
create index ix_favorite_user_1 on favorite (user_user_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table article;

drop table favorite;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

