// @SOURCE:/Users/tiagoaap/Projects/newslserver/conf/routes
// @HASH:dfdce394c15bc38717386d6812b8049c3f640897
// @DATE:Wed Jun 05 20:12:03 WEST 2013

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:33
// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:12
// @LINE:11
package controllers {

// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:12
// @LINE:11
class ReverseApplication {
    

// @LINE:17
def latestNews(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "latestnews")
}
                                                

// @LINE:26
def loginFacebook(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "loginfacebook")
}
                                                

// @LINE:29
def login(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "login")
}
                                                

// @LINE:27
def autocomplete(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "autocomplete")
}
                                                

// @LINE:11
def compile(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "compile")
}
                                                

// @LINE:30
def searchByCategory(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "searchbycategory")
}
                                                

// @LINE:18
// @LINE:16
def index(): Call = {
   () match {
// @LINE:16
case () if true => Call("GET", _prefix + { _defaultPrefix } + "index")
                                                        
// @LINE:18
case () if true => Call("GET", _prefix)
                                                        
   }
}
                                                

// @LINE:19
def search(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "search")
}
                                                

// @LINE:28
def registerApp(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "registerApp")
}
                                                

// @LINE:22
def article(articleId:Int): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "article/" + implicitly[PathBindable[Int]].unbind("articleId", articleId))
}
                                                

// @LINE:12
def indexing(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "indexing")
}
                                                
    
}
                          

// @LINE:33
class ReverseAssets {
    

// @LINE:33
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          
}
                  


// @LINE:33
// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:12
// @LINE:11
package controllers.javascript {

// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:12
// @LINE:11
class ReverseApplication {
    

// @LINE:17
def latestNews : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.latestNews",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "latestnews"})
      }
   """
)
                        

// @LINE:26
def loginFacebook : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.loginFacebook",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "loginfacebook"})
      }
   """
)
                        

// @LINE:29
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.login",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        

// @LINE:27
def autocomplete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.autocomplete",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "autocomplete"})
      }
   """
)
                        

// @LINE:11
def compile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.compile",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "compile"})
      }
   """
)
                        

// @LINE:30
def searchByCategory : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.searchByCategory",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchbycategory"})
      }
   """
)
                        

// @LINE:18
// @LINE:16
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "index"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
      }
   """
)
                        

// @LINE:19
def search : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.search",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "search"})
      }
   """
)
                        

// @LINE:28
def registerApp : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.registerApp",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "registerApp"})
      }
   """
)
                        

// @LINE:22
def article : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.article",
   """
      function(articleId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "article/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("articleId", articleId)})
      }
   """
)
                        

// @LINE:12
def indexing : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.indexing",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "indexing"})
      }
   """
)
                        
    
}
              

// @LINE:33
class ReverseAssets {
    

// @LINE:33
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              
}
        


// @LINE:33
// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:12
// @LINE:11
package controllers.ref {

// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:12
// @LINE:11
class ReverseApplication {
    

// @LINE:17
def latestNews(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.latestNews(), HandlerDef(this, "controllers.Application", "latestNews", Seq(), "GET", """""", _prefix + """latestnews""")
)
                      

// @LINE:26
def loginFacebook(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.loginFacebook(), HandlerDef(this, "controllers.Application", "loginFacebook", Seq(), "POST", """Requests on the fly""", _prefix + """loginfacebook""")
)
                      

// @LINE:29
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Seq(), "POST", """""", _prefix + """login""")
)
                      

// @LINE:27
def autocomplete(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.autocomplete(), HandlerDef(this, "controllers.Application", "autocomplete", Seq(), "POST", """""", _prefix + """autocomplete""")
)
                      

// @LINE:11
def compile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.compile(), HandlerDef(this, "controllers.Application", "compile", Seq(), "GET", """GET	/scrapingexpresso                           	controllers.Expresso.index()
GET	/scrapingdn                           			controllers.DiarioNoticias.index()
GET	/scraping										controllers.Application.scraping()""", _prefix + """compile""")
)
                      

// @LINE:30
def searchByCategory(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.searchByCategory(), HandlerDef(this, "controllers.Application", "searchByCategory", Seq(), "POST", """""", _prefix + """searchbycategory""")
)
                      

// @LINE:16
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """WebSiteStuff""", _prefix + """index""")
)
                      

// @LINE:19
def search(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.search(), HandlerDef(this, "controllers.Application", "search", Seq(), "GET", """""", _prefix + """search""")
)
                      

// @LINE:28
def registerApp(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.registerApp(), HandlerDef(this, "controllers.Application", "registerApp", Seq(), "POST", """""", _prefix + """registerApp""")
)
                      

// @LINE:22
def article(articleId:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.article(articleId), HandlerDef(this, "controllers.Application", "article", Seq(classOf[Int]), "GET", """""", _prefix + """article/$articleId<[^/]+>""")
)
                      

// @LINE:12
def indexing(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.indexing(), HandlerDef(this, "controllers.Application", "indexing", Seq(), "GET", """""", _prefix + """indexing""")
)
                      
    
}
                          

// @LINE:33
class ReverseAssets {
    

// @LINE:33
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          
}
                  
      