
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object login_register extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.Html] {

    /**/
    def apply():play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>NewsL - Sign In/Register</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<script src=""""),_display_(Seq[Any](/*11.15*/routes/*11.21*/.Assets.at("javascripts/jquery.js"))),format.raw/*11.56*/("""" type="text/javascript"></script>
<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*12.46*/routes/*12.52*/.Assets.at("stylesheets/bootstrap.css"))),format.raw/*12.91*/("""">
<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*13.46*/routes/*13.52*/.Assets.at("stylesheets/main.css"))),format.raw/*13.86*/("""">



<!--  <script src="assets/javascripts/bootstrap-transition.js"></script>
<script src="assets/javascripts/bootstrap-alert.js"></script>
<script src="assets/javascripts/bootstrap-modal.js"></script>
<script src="assets/javascripts/bootstrap-dropdown.js"></script>
<script src="assets/javascripts/bootstrap-scrollspy.js"></script>
<script src="assets/javascripts/bootstrap-tab.js"></script>
<script src="assets/javascripts/bootstrap-tooltip.js"></script>
<script src="assets/javascripts/bootstrap-popover.js"></script>
<script src="assets/javascripts/bootstrap-button.js"></script>
<script src="assets/javascripts/bootstrap-collapse.js"></script>
<script src="assets/javascripts/bootstrap-carousel.js"></script>
<script src="assets/javascripts/bootstrap-typeahead.js"></script>-->

</head>
<style type="text/css">
body """),format.raw/*32.6*/("""{"""),format.raw/*32.7*/("""
	padding-top: 20px;
	padding-bottom: 60px;
"""),format.raw/*35.1*/("""}"""),format.raw/*35.2*/("""

Custom container 
      .container """),format.raw/*38.18*/("""{"""),format.raw/*38.19*/("""
	margin: 0 auto;
	max-width: 1000px;
"""),format.raw/*41.1*/("""}"""),format.raw/*41.2*/("""

.container>hr """),format.raw/*43.15*/("""{"""),format.raw/*43.16*/("""
	margin: 60px 0;
"""),format.raw/*45.1*/("""}"""),format.raw/*45.2*/("""

/* Main marketing message and sign up button */
.jumbotron """),format.raw/*48.12*/("""{"""),format.raw/*48.13*/("""
	margin: 80px 0;
	text-align: center;
"""),format.raw/*51.1*/("""}"""),format.raw/*51.2*/("""

.jumbotron h1 """),format.raw/*53.15*/("""{"""),format.raw/*53.16*/("""
	font-size: 40px;
	line-height: 1;
"""),format.raw/*56.1*/("""}"""),format.raw/*56.2*/("""

.jumbotron .lead """),format.raw/*58.18*/("""{"""),format.raw/*58.19*/("""
	font-size: 24px;
	line-height: 1.25;
"""),format.raw/*61.1*/("""}"""),format.raw/*61.2*/("""

.jumbotron .btn """),format.raw/*63.17*/("""{"""),format.raw/*63.18*/("""
	font-size: 21px;
	padding: 14px 24px;
"""),format.raw/*66.1*/("""}"""),format.raw/*66.2*/("""

/* Supporting marketing content */
.marketing """),format.raw/*69.12*/("""{"""),format.raw/*69.13*/("""
	margin: 60px 0;
"""),format.raw/*71.1*/("""}"""),format.raw/*71.2*/("""

.marketing p+h4 """),format.raw/*73.17*/("""{"""),format.raw/*73.18*/("""
	margin-top: 28px;
"""),format.raw/*75.1*/("""}"""),format.raw/*75.2*/("""
</style>
</head>

<body>
<script>
  window.fbAsyncInit = function() """),format.raw/*81.35*/("""{"""),format.raw/*81.36*/("""
  FB.init("""),format.raw/*82.11*/("""{"""),format.raw/*82.12*/("""
    appId      : '548195871914194', // App ID
    //channelUrl : '"""),_display_(Seq[Any](/*84.22*/routes/*84.28*/.Assets.at("html/channel.html"))),format.raw/*84.59*/("""', // Channel File
    channelUrl : 'http://newsl.dei.uc.pt:8080/assets/html/channel.html")', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  """),format.raw/*89.3*/("""}"""),format.raw/*89.4*/(""");

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) """),format.raw/*95.68*/("""{"""),format.raw/*95.69*/("""
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') """),format.raw/*97.42*/("""{"""),format.raw/*97.43*/("""
      alert('login successful');
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    """),format.raw/*103.5*/("""}"""),format.raw/*103.6*/(""" else if (response.status === 'not_authorized') """),format.raw/*103.54*/("""{"""),format.raw/*103.55*/("""
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    """),format.raw/*112.5*/("""}"""),format.raw/*112.6*/(""" else """),format.raw/*112.12*/("""{"""),format.raw/*112.13*/("""
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    """),format.raw/*119.5*/("""}"""),format.raw/*119.6*/("""
  """),format.raw/*120.3*/("""}"""),format.raw/*120.4*/(""");
  """),format.raw/*121.3*/("""}"""),format.raw/*121.4*/(""";

  // Load the SDK asynchronously
  (function(d)"""),format.raw/*124.15*/("""{"""),format.raw/*124.16*/("""
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) """),format.raw/*126.30*/("""{"""),format.raw/*126.31*/("""return;"""),format.raw/*126.38*/("""}"""),format.raw/*126.39*/("""
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "http://connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  """),format.raw/*130.3*/("""}"""),format.raw/*130.4*/("""(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() """),format.raw/*134.22*/("""{"""),format.raw/*134.23*/("""
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) """),format.raw/*136.38*/("""{"""),format.raw/*136.39*/("""
      console.log('Good to see you, ' + response.name + '.');
      console.log('Good to see you, ' + response.id + '.');
      
      $.post('/loginfacebook', """),format.raw/*140.32*/("""{"""),format.raw/*140.33*/("""'id':response.id"""),format.raw/*140.49*/("""}"""),format.raw/*140.50*/(""", function(data) """),format.raw/*140.67*/("""{"""),format.raw/*140.68*/("""
    	  alert(data);
      
      """),format.raw/*143.7*/("""}"""),format.raw/*143.8*/(""");
    
    """),format.raw/*145.5*/("""}"""),format.raw/*145.6*/(""");
  """),format.raw/*146.3*/("""}"""),format.raw/*146.4*/("""
</script>

<!--
  Below we include the Login Button social plugin. This button uses the JavaScript SDK to
  present a graphical Login button that triggers the FB.login() function when clicked.

  Learn more about options for the login button plugin:
  /docs/reference/plugins/login/ -->

<fb:login-button show-faces="true" width="200" max-rows="1"></fb:login-button>
	<div class="container">
		<div class="span6">
			<h3 class="form-signin-heading">Iniciar Sessão</h3>
			<form class="form-signin">
			<input type="text" class="input-block-level"
				placeholder="Endereço de Email"> <input type="password"
				class="input-block-level" placeholder="Palavra-Passe"> <label
				class="checkbox"> <input type="checkbox" value="remember-me">Lembrar-me
			</label>
			<button class="btn btn-info" type="submit">Entrar</button>
			</form>
			
		</div>
		<!-- 		<div class="span6 well">
			<h3 class="form-signin-heading">Novo no NewsL? Increva-se</h3>
			<form accept-charset="UTF-8" action="" method="post">
				<input class="span3" name="name" placeholder="Full Name" type="text">
				<input class="span3" name="username" placeholder="Username" type="text"> 
				<input class="span3" name="password"
					placeholder="Password" type="password">
				<button class="btn btn-info" type="submit">Sign up for
					WebApp</button>
			</form>
		</div> -->
		

	</div>


</body>
</html>
"""))}
    }
    
    def render(): play.api.templates.Html = apply()
    
    def f:(() => play.api.templates.Html) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jun 05 20:12:06 WEST 2013
                    SOURCE: /Users/tiagoaap/Projects/newslserver/app/views/login_register.scala.html
                    HASH: 12300130211fd8e83f5d2cfa1e262cb53a367180
                    MATRIX: 796->0|1109->277|1124->283|1181->318|1297->398|1312->404|1373->443|1457->491|1472->497|1528->531|2377->1353|2405->1354|2476->1398|2504->1399|2569->1436|2598->1437|2663->1475|2691->1476|2735->1492|2764->1493|2809->1511|2837->1512|2926->1573|2955->1574|3021->1613|3049->1614|3093->1630|3122->1631|3185->1667|3213->1668|3260->1687|3289->1688|3355->1727|3383->1728|3429->1746|3458->1747|3525->1787|3553->1788|3629->1836|3658->1837|3703->1855|3731->1856|3777->1874|3806->1875|3853->1895|3881->1896|3978->1965|4007->1966|4046->1977|4075->1978|4179->2046|4194->2052|4247->2083|4552->2361|4580->2362|4995->2749|5024->2750|5174->2872|5203->2873|5510->3152|5539->3153|5616->3201|5646->3202|6225->3753|6254->3754|6289->3760|6319->3761|6763->4177|6792->4178|6823->4181|6852->4182|6885->4187|6914->4188|6993->4238|7023->4239|7159->4346|7189->4347|7225->4354|7255->4355|7447->4519|7476->4520|7682->4697|7712->4698|7840->4797|7870->4798|8060->4959|8090->4960|8135->4976|8165->4977|8211->4994|8241->4995|8303->5029|8332->5030|8372->5042|8401->5043|8434->5048|8463->5049
                    LINES: 29->1|39->11|39->11|39->11|40->12|40->12|40->12|41->13|41->13|41->13|60->32|60->32|63->35|63->35|66->38|66->38|69->41|69->41|71->43|71->43|73->45|73->45|76->48|76->48|79->51|79->51|81->53|81->53|84->56|84->56|86->58|86->58|89->61|89->61|91->63|91->63|94->66|94->66|97->69|97->69|99->71|99->71|101->73|101->73|103->75|103->75|109->81|109->81|110->82|110->82|112->84|112->84|112->84|117->89|117->89|123->95|123->95|125->97|125->97|131->103|131->103|131->103|131->103|140->112|140->112|140->112|140->112|147->119|147->119|148->120|148->120|149->121|149->121|152->124|152->124|154->126|154->126|154->126|154->126|158->130|158->130|162->134|162->134|164->136|164->136|168->140|168->140|168->140|168->140|168->140|168->140|171->143|171->143|173->145|173->145|174->146|174->146
                    -- GENERATED --
                */
            