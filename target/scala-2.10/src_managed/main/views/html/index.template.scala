
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template1[String,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(message: String):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.19*/(""" """),_display_(Seq[Any](/*1.21*/main(message)/*1.34*/ {_display_(Seq[Any](format.raw/*1.36*/("""
<meta charset="utf-8">
<div class="jumbotron">
	<h1>Bem Vindo ao NewsL</h1>
	<p class="lead">Toda a informação nacional e internacional num só
		site</p>
	"""),_display_(Seq[Any](/*7.3*/if(session.get("name")==null)/*7.32*/{_display_(Seq[Any](format.raw/*7.33*/("""
		<a id="registerBtn" href="#registerModal"class="btn btn-large btn-info" data-toggle="modal">Adira agora</a><br>
		<a class="btn" href="#loginModal" style="margin-top:25px;font-size: 14px; padding: 4px 12px;"data-toggle="modal">Inicie a sessão</a>
	""")))})),format.raw/*10.3*/("""
</div>
<div class="row-fluid">
	<div class="span4" style="">
		<h4>Marcelo diz que Franquelim deve explicar "todas as dúvidas" no Parlamento</h4>
		<p>O conselho é de Marcelo Rebelo de Sousa. "Quem não deve não teme", diz o comentador político.</p>
		<p>
			<a class="btn btn-info" href="#">Ver detalhes &raquo;</a>
		</p>
	</div>
	<div class="span4">
		<h4>Líder do BE indignado por Cavaco dar posse a Franquelim Alves</h4>
		<p>João Semedo diz que o país está "indignado", "revoltado" e "escandalizado" com a nomeação deum secretário de Estado para o Empreendedorismo que vem da administração do grupo BPN/SLN.</p>
		<p>
			<a class="btn btn-info" href="#">Ver detalhes &raquo;</a>
		</p>
	</div>
	<div class="span4">
		<h4>Franquelim Alves "perfeitamente tranquilo" com entrada no Governo</h4>
		<p>Novo secretário de Estado do Empreendedorismo diz que o seu envolvimento com o BPN é uma "falsa questão".</p>
		<p>
			<a class="btn btn-info" href="#">Ver detalhes &raquo;</a>
		</p>
	</div>
</div>



""")))})),format.raw/*38.2*/("""
	<div id="registerModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=width:420px;margin-left:-220px;background-color:#F8F8F8;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">×</button>
			<h3 id="myModalLabel" style="margin-left:5px;">Registe-se agora</h3>
		</div>
		<div class="span5">
			<input id="name" type="text" style="margin-top:10px;"class="input-block-level" placeholder="Nome Completo">
			<input id="email" type="email" style="margin-top:10px;"class="input-block-level" placeholder="Endereço de Email">
			
			<div class="input-append date" id="dpYears" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years" style="margin-top:10px;">
				<input class="span3" id="birthdate" size="16" type="text" value="12-02-2012" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			</div>
			<input id="password" type="password" style="margin-top:10px;" class="input-block-level" placeholder="Palavra-Passe">
			<input id="retype" type="password" style="margin-top:10px;" class="input-block-level" placeholder="Repita a Palavra-Passe">
			
			<div class="modal-footer" style="float:left;margin-left:-15px;">
				<button class="btn btn-info" type="submit" id="register" style="width:100px;">Registar</button>
    			<button class="btn btn-primary" data-dismiss="modal" style="width:100px;">Fechar</button>
				
  			</div>
  			<div class="alert alert-error" id="errorMessage" style="margin-top:55px;">
  			
				
			</div>
  			
		</div>
		
	</div>
	<div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=width:420px;margin-left:-220px;background-color:#F8F8F8;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">×</button>
			<h3 id="myModalLabel" style="margin-left:5px;">Inicie a sua sessão</h3>
		</div>
		<div class="span5">
			<input id="emailLogin" type="email" style="margin-top:10px;"class="input-block-level" placeholder="Endereço de Email">
			<input id="passwordLogin" type="password" style="margin-top:10px;" class="input-block-level" placeholder="Palavra-Passe">
			
			<div class="modal-footer" style="float:left;margin-left:-15px;">
				<button class="btn btn-info" type="submit" id="login" style="width:100px;">Entrar</button>
    			<button class="btn btn-primary" data-dismiss="modal" style="width:100px;">Fechar</button>
				
  			</div>
  			<div class="alert alert-error" id="errorMessageLogin" style="margin-top:55px;">
  			
				
			</div>
  			
		</div>
		
	</div>
	
	<script>
	
	
		$(function()"""),format.raw/*96.15*/("""{"""),format.raw/*96.16*/("""
			$("#errorMessage").hide();
			$("#errorMessageLogin").hide();
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker("""),format.raw/*100.25*/("""{"""),format.raw/*100.26*/("""
				format: 'mm-dd-yyyy'
			"""),format.raw/*102.4*/("""}"""),format.raw/*102.5*/(""");
			$('#dp2').datepicker();
			$('#dp3').datepicker();
			$('#dp3').datepicker();
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev)"""),format.raw/*113.35*/("""{"""),format.raw/*113.36*/("""
					if (ev.date.valueOf() > endDate.valueOf())"""),format.raw/*114.48*/("""{"""),format.raw/*114.49*/("""
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					"""),format.raw/*116.6*/("""}"""),format.raw/*116.7*/(""" else """),format.raw/*116.13*/("""{"""),format.raw/*116.14*/("""
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					"""),format.raw/*120.6*/("""}"""),format.raw/*120.7*/("""
					$('#dp4').datepicker('hide');
				"""),format.raw/*122.5*/("""}"""),format.raw/*122.6*/(""");
			$('#dp5').datepicker()
				.on('changeDate', function(ev)"""),format.raw/*124.35*/("""{"""),format.raw/*124.36*/("""
					if (ev.date.valueOf() < startDate.valueOf())"""),format.raw/*125.50*/("""{"""),format.raw/*125.51*/("""
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					"""),format.raw/*127.6*/("""}"""),format.raw/*127.7*/(""" else """),format.raw/*127.13*/("""{"""),format.raw/*127.14*/("""
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					"""),format.raw/*131.6*/("""}"""),format.raw/*131.7*/("""
					$('#dp5').datepicker('hide');
				"""),format.raw/*133.5*/("""}"""),format.raw/*133.6*/(""");

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker("""),format.raw/*139.45*/("""{"""),format.raw/*139.46*/("""
          onRender: function(date) """),format.raw/*140.36*/("""{"""),format.raw/*140.37*/("""
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          """),format.raw/*142.11*/("""}"""),format.raw/*142.12*/("""
        """),format.raw/*143.9*/("""}"""),format.raw/*143.10*/(""").on('changeDate', function(ev) """),format.raw/*143.42*/("""{"""),format.raw/*143.43*/("""
          if (ev.date.valueOf() > checkout.date.valueOf()) """),format.raw/*144.60*/("""{"""),format.raw/*144.61*/("""
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          """),format.raw/*148.11*/("""}"""),format.raw/*148.12*/("""
          checkin.hide();
          $('#dpd2')[0].focus();
        """),format.raw/*151.9*/("""}"""),format.raw/*151.10*/(""").data('datepicker');
        var checkout = $('#dpd2').datepicker("""),format.raw/*152.46*/("""{"""),format.raw/*152.47*/("""
          onRender: function(date) """),format.raw/*153.36*/("""{"""),format.raw/*153.37*/("""
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          """),format.raw/*155.11*/("""}"""),format.raw/*155.12*/("""
        """),format.raw/*156.9*/("""}"""),format.raw/*156.10*/(""").on('changeDate', function(ev) """),format.raw/*156.42*/("""{"""),format.raw/*156.43*/("""
          checkout.hide();
        """),format.raw/*158.9*/("""}"""),format.raw/*158.10*/(""").data('datepicker');
		"""),format.raw/*159.3*/("""}"""),format.raw/*159.4*/(""");
		
		
		
		$("#register").click(function()"""),format.raw/*163.34*/("""{"""),format.raw/*163.35*/("""
			$.post('/registerApp', """),format.raw/*164.27*/("""{"""),format.raw/*164.28*/("""'name':$("#name").val(),'email':$("#email").val(),'dpYears':$("#birthdate").val(),
				'password':$("#password").val(),'retype':$("#retype").val()"""),format.raw/*165.64*/("""}"""),format.raw/*165.65*/(""", function(data) """),format.raw/*165.82*/("""{"""),format.raw/*165.83*/("""
		      	if(data=="pdm")"""),format.raw/*166.25*/("""{"""),format.raw/*166.26*/("""
		      		$("#errorMessage").attr('class','alert alert-error');
		      		$("#errorMessage").html("<button type='button' class='close' id='closeBtn'>×</button><p style='margin-top:10px;'><b>Erro: </b>As palavras passe não coincidem!</p>");
		      		$("#errorMessage").show();
		      		$("#closeBtn").click(function()"""),format.raw/*170.42*/("""{"""),format.raw/*170.43*/("""
		    			$("#errorMessage").hide();
		    		"""),format.raw/*172.9*/("""}"""),format.raw/*172.10*/(""");
		      		
		      	"""),format.raw/*174.10*/("""}"""),format.raw/*174.11*/("""
		      	else if(data=="ie")"""),format.raw/*175.29*/("""{"""),format.raw/*175.30*/("""
		      		$("#errorMessage").attr('class','alert alert-error');
		      		$("#errorMessage").html("<button type='button' class='close' id='closeBtn'>×</button><p style='margin-top:10px;'><b>Erro: </b>Endereço de email inválido!</p>");
		      		$("#errorMessage").show();
		      		$("#closeBtn").click(function()"""),format.raw/*179.42*/("""{"""),format.raw/*179.43*/("""
		    			$("#errorMessage").hide();
		    		"""),format.raw/*181.9*/("""}"""),format.raw/*181.10*/(""");
		      		
		      	"""),format.raw/*183.10*/("""}"""),format.raw/*183.11*/("""
		      	else if(data=="uae")"""),format.raw/*184.30*/("""{"""),format.raw/*184.31*/("""
		      		$("#errorMessage").attr('class','alert alert-error');
		      		$("#errorMessage").html("<button type='button' class='close' id='closeBtn'>×</button><p style='margin-top:10px;'><b>Erro: </b>Já existe um utilizador que utiliza este email!</p>");
		      		$("#errorMessage").show();
		      		$("#closeBtn").click(function()"""),format.raw/*188.42*/("""{"""),format.raw/*188.43*/("""
		    			$("#errorMessage").hide();
		    		"""),format.raw/*190.9*/("""}"""),format.raw/*190.10*/(""");
		      		
		      	"""),format.raw/*192.10*/("""}"""),format.raw/*192.11*/("""
		      	else if(data=="in")"""),format.raw/*193.29*/("""{"""),format.raw/*193.30*/("""
		      		$("#errorMessage").attr('class','alert alert-error');
		      		$("#errorMessage").html("<button type='button' class='close' id='closeBtn'>×</button><p style='margin-top:10px;'><b>Erro: </b>Nome inválido!</p>");
		      		$("#errorMessage").show();
		      		$("#closeBtn").click(function()"""),format.raw/*197.42*/("""{"""),format.raw/*197.43*/("""
		    			$("#errorMessage").hide();
		    		"""),format.raw/*199.9*/("""}"""),format.raw/*199.10*/(""");
		      		
		      	"""),format.raw/*201.10*/("""}"""),format.raw/*201.11*/("""
		      	else if(data=="pts")"""),format.raw/*202.30*/("""{"""),format.raw/*202.31*/("""
		      		$("#errorMessage").attr('class','alert alert-error');
		      		$("#errorMessage").html("<button type='button' class='close' id='closeBtn'>×</button><p style='margin-top:10px;'><b>Erro: </b>Palavra passe muito curta!</p>");
		      		$("#errorMessage").show();
		      		$("#closeBtn").click(function()"""),format.raw/*206.42*/("""{"""),format.raw/*206.43*/("""
		    			$("#errorMessage").hide();
		    		"""),format.raw/*208.9*/("""}"""),format.raw/*208.10*/(""");
		      		
		      	"""),format.raw/*210.10*/("""}"""),format.raw/*210.11*/("""
		      	else if(data=="created")"""),format.raw/*211.34*/("""{"""),format.raw/*211.35*/("""
		      		$("#errorMessage").attr('class','alert alert-success');
		      		$("#errorMessage").html("<button type='button' class='close' id='closeBtn'>×</button><p style='margin-top:10px;'><b>Informação: </b>Utilizador criado com sucesso!</p>");
		      		$("#errorMessage").show();
		      		$("#closeBtn").click(function()"""),format.raw/*215.42*/("""{"""),format.raw/*215.43*/("""
		    			$("#errorMessage").hide();
		    		"""),format.raw/*217.9*/("""}"""),format.raw/*217.10*/(""");
		      		$("#register").attr('class','btn btn-info disabled');
		      		$("#register").attr('disabled',true);
		      		setTimeout('timeout_trigger()', 3000);
		      		
		      	"""),format.raw/*222.10*/("""}"""),format.raw/*222.11*/("""
		      """),format.raw/*223.9*/("""}"""),format.raw/*223.10*/(""");
		"""),format.raw/*224.3*/("""}"""),format.raw/*224.4*/(""");
		function timeout_trigger() """),format.raw/*225.30*/("""{"""),format.raw/*225.31*/("""
			$("#register").attr('class','btn btn-info');
			$("#register").attr('disabled',false);
			$("#registerModal").modal('hide');
		"""),format.raw/*229.3*/("""}"""),format.raw/*229.4*/("""
		
		$("#login").click(function()"""),format.raw/*231.31*/("""{"""),format.raw/*231.32*/("""
			$.post('/login', """),format.raw/*232.21*/("""{"""),format.raw/*232.22*/("""'email':$("#emailLogin").val(),'password':$("#passwordLogin").val()"""),format.raw/*232.89*/("""}"""),format.raw/*232.90*/(""", function(data) """),format.raw/*232.107*/("""{"""),format.raw/*232.108*/("""
		      	if(data=="ude")"""),format.raw/*233.25*/("""{"""),format.raw/*233.26*/("""
		      		$("#errorMessageLogin").attr('class','alert alert-error');
		      		$("#errorMessageLogin").html("<button type='button' class='close' id='closeBtnLogin'>×</button><p style='margin-top:10px;'><b>Erro: </b>O utilizador não existe!</p>");
		      		$("#errorMessageLogin").show();
		      		$("#closeBtnLogin").click(function()"""),format.raw/*237.47*/("""{"""),format.raw/*237.48*/("""
		    			$("#errorMessageLogin").hide();
		    		"""),format.raw/*239.9*/("""}"""),format.raw/*239.10*/(""");
		      		
		      	"""),format.raw/*241.10*/("""}"""),format.raw/*241.11*/("""
		      	else if(data=="wp")"""),format.raw/*242.29*/("""{"""),format.raw/*242.30*/("""
		      		$("#errorMessageLogin").attr('class','alert alert-error');
		      		$("#errorMessageLogin").html("<button type='button' class='close' id='closeBtnLogin'>×</button><p style='margin-top:10px;'><b>Erro: </b>A palavra passe está errada!</p>");
		      		$("#errorMessageLogin").show();
		      		$("#closeBtnLogin").click(function()"""),format.raw/*246.47*/("""{"""),format.raw/*246.48*/("""
		    			$("#errorMessageLogin").hide();
		    		"""),format.raw/*248.9*/("""}"""),format.raw/*248.10*/(""");
		      		
		      	"""),format.raw/*250.10*/("""}"""),format.raw/*250.11*/("""
		      	else if(data=="ls")"""),format.raw/*251.29*/("""{"""),format.raw/*251.30*/("""
		      		window.location.replace("/index");
					
		      		
		      	"""),format.raw/*255.10*/("""}"""),format.raw/*255.11*/("""
		      """),format.raw/*256.9*/("""}"""),format.raw/*256.10*/(""");
		"""),format.raw/*257.3*/("""}"""),format.raw/*257.4*/(""");
		
		
		
		
		
		
	</script>

"""))}
    }
    
    def render(message:String): play.api.templates.Html = apply(message)
    
    def f:((String) => play.api.templates.Html) = (message) => apply(message)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jun 05 20:12:06 WEST 2013
                    SOURCE: /Users/tiagoaap/Projects/newslserver/app/views/index.scala.html
                    HASH: ec66582f73ee48e46a0edae17fb59b820ddae137
                    MATRIX: 723->1|817->18|854->20|875->33|914->35|1105->192|1142->221|1180->222|1463->474|2500->1480|5263->4215|5292->4216|5451->4346|5481->4347|5538->4376|5567->4377|5889->4670|5919->4671|5996->4719|6026->4720|6161->4827|6190->4828|6225->4834|6255->4835|6404->4956|6433->4957|6501->4997|6530->4998|6622->5061|6652->5062|6731->5112|6761->5113|6893->5217|6922->5218|6957->5224|6987->5225|7132->5342|7161->5343|7229->5383|7258->5384|7499->5596|7529->5597|7594->5633|7624->5634|7733->5714|7763->5715|7800->5724|7830->5725|7891->5757|7921->5758|8010->5818|8040->5819|8216->5966|8246->5967|8342->6035|8372->6036|8468->6103|8498->6104|8563->6140|8593->6141|8712->6231|8742->6232|8779->6241|8809->6242|8870->6274|8900->6275|8964->6311|8994->6312|9046->6336|9075->6337|9149->6382|9179->6383|9235->6410|9265->6411|9440->6557|9470->6558|9516->6575|9546->6576|9600->6601|9630->6602|9978->6921|10008->6922|10081->6967|10111->6968|10163->6991|10193->6992|10251->7021|10281->7022|10624->7336|10654->7337|10727->7382|10757->7383|10809->7406|10839->7407|10898->7437|10928->7438|11291->7772|11321->7773|11394->7818|11424->7819|11476->7842|11506->7843|11564->7872|11594->7873|11924->8174|11954->8175|12027->8220|12057->8221|12109->8244|12139->8245|12198->8275|12228->8276|12570->8589|12600->8590|12673->8635|12703->8636|12755->8659|12785->8660|12848->8694|12878->8695|13232->9020|13262->9021|13335->9066|13365->9067|13578->9251|13608->9252|13645->9261|13675->9262|13708->9267|13737->9268|13798->9300|13828->9301|13987->9432|14016->9433|14079->9467|14109->9468|14159->9489|14189->9490|14285->9557|14315->9558|14362->9575|14393->9576|14447->9601|14477->9602|14842->9938|14872->9939|14950->9989|14980->9990|15032->10013|15062->10014|15120->10043|15150->10044|15519->10384|15549->10385|15627->10435|15657->10436|15709->10459|15739->10460|15797->10489|15827->10490|15928->10562|15958->10563|15995->10572|16025->10573|16058->10578|16087->10579
                    LINES: 26->1|29->1|29->1|29->1|29->1|35->7|35->7|35->7|38->10|66->38|124->96|124->96|128->100|128->100|130->102|130->102|141->113|141->113|142->114|142->114|144->116|144->116|144->116|144->116|148->120|148->120|150->122|150->122|152->124|152->124|153->125|153->125|155->127|155->127|155->127|155->127|159->131|159->131|161->133|161->133|167->139|167->139|168->140|168->140|170->142|170->142|171->143|171->143|171->143|171->143|172->144|172->144|176->148|176->148|179->151|179->151|180->152|180->152|181->153|181->153|183->155|183->155|184->156|184->156|184->156|184->156|186->158|186->158|187->159|187->159|191->163|191->163|192->164|192->164|193->165|193->165|193->165|193->165|194->166|194->166|198->170|198->170|200->172|200->172|202->174|202->174|203->175|203->175|207->179|207->179|209->181|209->181|211->183|211->183|212->184|212->184|216->188|216->188|218->190|218->190|220->192|220->192|221->193|221->193|225->197|225->197|227->199|227->199|229->201|229->201|230->202|230->202|234->206|234->206|236->208|236->208|238->210|238->210|239->211|239->211|243->215|243->215|245->217|245->217|250->222|250->222|251->223|251->223|252->224|252->224|253->225|253->225|257->229|257->229|259->231|259->231|260->232|260->232|260->232|260->232|260->232|260->232|261->233|261->233|265->237|265->237|267->239|267->239|269->241|269->241|270->242|270->242|274->246|274->246|276->248|276->248|278->250|278->250|279->251|279->251|283->255|283->255|284->256|284->256|285->257|285->257
                    -- GENERATED --
                */
            