
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object show_article extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Article,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(article :Article):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.20*/(""" 
"""),_display_(Seq[Any](/*2.2*/main(article.getTitle())/*2.26*/ {_display_(Seq[Any](format.raw/*2.28*/("""
	<meta charset="utf-8">
	"""),_display_(Seq[Any](/*4.3*/if(article!=null)/*4.20*/{_display_(Seq[Any](format.raw/*4.21*/("""
   		<h3>"""),_display_(Seq[Any](/*5.11*/article/*5.18*/.getTitle())),format.raw/*5.29*/("""<br></h3>
   		<i>"""),_display_(Seq[Any](/*6.10*/article/*6.17*/.getAuthor())),format.raw/*6.29*/(""", """),_display_(Seq[Any](/*6.32*/article/*6.39*/.getFormattedDate())),format.raw/*6.58*/("""<br><br></i>
   		<p style="color:#4d6ba4">"""),_display_(Seq[Any](/*7.32*/article/*7.39*/.getSummary())),format.raw/*7.52*/("""<br><br></p>
   		<div id="columns">
   			<p style="text-align:justify;">"""),_display_(Seq[Any](/*9.39*/article/*9.46*/.getBody())),format.raw/*9.56*/("""</p>
   		</div>
   		<hr>
   	""")))})),format.raw/*12.6*/("""
""")))})))}
    }
    
    def render(article:Article): play.api.templates.Html = apply(article)
    
    def f:((Article) => play.api.templates.Html) = (article) => apply(article)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jun 05 20:12:06 WEST 2013
                    SOURCE: /Users/tiagoaap/Projects/newslserver/app/views/show_article.scala.html
                    HASH: bedd71bb276cfa2f31bc6cc251065e1352651b74
                    MATRIX: 731->1|826->19|863->22|895->46|934->48|995->75|1020->92|1058->93|1104->104|1119->111|1151->122|1205->141|1220->148|1253->160|1291->163|1306->170|1346->189|1425->233|1440->240|1474->253|1584->328|1599->335|1630->345|1693->377
                    LINES: 26->1|29->1|30->2|30->2|30->2|32->4|32->4|32->4|33->5|33->5|33->5|34->6|34->6|34->6|34->6|34->6|34->6|35->7|35->7|35->7|37->9|37->9|37->9|40->12
                    -- GENERATED --
                */
            