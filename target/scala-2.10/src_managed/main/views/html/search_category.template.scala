
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object search_category extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[Message],String,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(articleResults :List[Message], searchQuery :String):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.54*/("""{"""),format.raw/*1.55*/("""
	<meta charset="utf-8">
  	"""),_display_(Seq[Any](/*3.5*/if(articleResults==null)/*3.29*/{_display_(Seq[Any](format.raw/*3.30*/("""
   		<h4 id="searchHeaders" style="margin-top:0px;">Não foram encontrados resultados!<br></h4>
   		
   	""")))})),format.raw/*6.6*/("""
   	"""),_display_(Seq[Any](/*7.6*/if(articleResults!=null)/*7.30*/{_display_(Seq[Any](format.raw/*7.31*/("""
   		"""),_display_(Seq[Any](/*8.7*/if(searchQuery=="politicslastday")/*8.41*/{_display_(Seq[Any](format.raw/*8.42*/("""
   			<h4 id="searchHeaders" style="margin-top:0px;">Notícias de política do último dia:<br></h4>
   		""")))})),format.raw/*10.7*/("""
   	""")))})),format.raw/*11.6*/("""
	"""),_display_(Seq[Any](/*12.3*/if(articleResults!= null && articleResults.size() > 0)/*12.57*/{_display_(Seq[Any](format.raw/*12.58*/("""
		<table class="table table-hover table-striped" id="searchResultsTable">
			"""),_display_(Seq[Any](/*14.5*/for(ArticleResult <- articleResults) yield /*14.41*/ {_display_(Seq[Any](format.raw/*14.43*/("""
				<tr>
					<td id="name_td">
						
						<a href="/article/"""),_display_(Seq[Any](/*18.26*/ArticleResult/*18.39*/.getArticle().getId())),format.raw/*18.60*/("""" style="font-size:16px;">"""),_display_(Seq[Any](/*18.87*/ArticleResult/*18.100*/.getArticle().getTitle())),format.raw/*18.124*/("""</a><br>
						<i style="font-size:12px;color:#000">"""),_display_(Seq[Any](/*19.45*/ArticleResult/*19.58*/.getArticle().getFormattedDate())),format.raw/*19.90*/(""", publicado em """),_display_(Seq[Any](/*19.106*/ArticleResult/*19.119*/.getArticle().getSource())),format.raw/*19.144*/(""" por """),_display_(Seq[Any](/*19.150*/ArticleResult/*19.163*/.getArticle().getAuthor())),format.raw/*19.188*/("""</i><br>
						<i style="font-size:12px;">"""),_display_(Seq[Any](/*20.35*/ArticleResult/*20.48*/.getArticle().getSummary())),format.raw/*20.74*/("""</i>
					</td>
				</tr>
			""")))})),format.raw/*23.5*/("""
		</table>
		
	""")))})),format.raw/*26.3*/("""
"""),format.raw/*27.1*/("""}"""))}
    }
    
    def render(articleResults:List[Message],searchQuery:String): play.api.templates.Html = apply(articleResults,searchQuery)
    
    def f:((List[Message],String) => play.api.templates.Html) = (articleResults,searchQuery) => apply(articleResults,searchQuery)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jun 05 20:12:06 WEST 2013
                    SOURCE: /Users/tiagoaap/Projects/newslserver/app/views/search_category.scala.html
                    HASH: 1d5f30d47177406079fff882170824294c10de3e
                    MATRIX: 747->1|876->53|904->54|967->83|999->107|1037->108|1174->215|1214->221|1246->245|1284->246|1325->253|1367->287|1405->288|1541->393|1578->399|1616->402|1679->456|1718->457|1832->536|1884->572|1924->574|2025->639|2047->652|2090->673|2153->700|2176->713|2223->737|2312->790|2334->803|2388->835|2441->851|2464->864|2512->889|2555->895|2578->908|2626->933|2705->976|2727->989|2775->1015|2836->1045|2884->1062|2912->1063
                    LINES: 26->1|29->1|29->1|31->3|31->3|31->3|34->6|35->7|35->7|35->7|36->8|36->8|36->8|38->10|39->11|40->12|40->12|40->12|42->14|42->14|42->14|46->18|46->18|46->18|46->18|46->18|46->18|47->19|47->19|47->19|47->19|47->19|47->19|47->19|47->19|47->19|48->20|48->20|48->20|51->23|54->26|55->27
                    -- GENERATED --
                */
            