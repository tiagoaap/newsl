
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object search_results extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[Message],String,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(articleResults :List[Message], searchQuery :String):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.54*/(""" """),_display_(Seq[Any](/*1.56*/main("Resultados da Pesquisa")/*1.86*/ {_display_(Seq[Any](format.raw/*1.88*/("""
	<meta charset="utf-8">
	"""),_display_(Seq[Any](/*3.3*/if(articleResults!=null)/*3.27*/{_display_(Seq[Any](format.raw/*3.28*/("""
   		<h4 id="searchHeaders" style="margin-top:30px;">Resultados para """"),_display_(Seq[Any](/*4.72*/searchQuery)),format.raw/*4.83*/("""":<br></h4>
   	""")))})),format.raw/*5.6*/("""
  		"""),_display_(Seq[Any](/*6.6*/if(articleResults==null)/*6.30*/{_display_(Seq[Any](format.raw/*6.31*/("""
   		<h4 id="searchHeaders" style="margin-top:30px;">Não foram encontrados resultados para """"),_display_(Seq[Any](/*7.94*/searchQuery)),format.raw/*7.105*/(""""!<br></h4>
   		<hr>
   	""")))})),format.raw/*9.6*/("""
	"""),_display_(Seq[Any](/*10.3*/if(articleResults!= null && articleResults.size() > 0)/*10.57*/{_display_(Seq[Any](format.raw/*10.58*/("""
		<table class="table table-hover table-striped" id="searchResultsTable">
			"""),_display_(Seq[Any](/*12.5*/for(ArticleResult <- articleResults) yield /*12.41*/ {_display_(Seq[Any](format.raw/*12.43*/("""
				<tr>
					<td id="name_td">
						
						<a href="/article/"""),_display_(Seq[Any](/*16.26*/ArticleResult/*16.39*/.getArticle().getId())),format.raw/*16.60*/("""" style="font-size:16px;">"""),_display_(Seq[Any](/*16.87*/ArticleResult/*16.100*/.getArticle().getTitle())),format.raw/*16.124*/("""</a><br>
						<i style="font-size:12px;color:#000">"""),_display_(Seq[Any](/*17.45*/ArticleResult/*17.58*/.getArticle().getFormattedDate())),format.raw/*17.90*/(""", publicado em """),_display_(Seq[Any](/*17.106*/ArticleResult/*17.119*/.getArticle().getSource())),format.raw/*17.144*/(""" por """),_display_(Seq[Any](/*17.150*/ArticleResult/*17.163*/.getArticle().getAuthor())),format.raw/*17.188*/("""</i><br>
						<!-- <i class="icon-tags"></i><i style="margin-left: 5px;font-size:12px;color:#000;">"""),_display_(Seq[Any](/*18.93*/ArticleResult/*18.106*/.getTokensStr())),format.raw/*18.121*/("""</i></br> -->
						"""),_display_(Seq[Any](/*19.8*/for(s <- ArticleResult.getTokens()) yield /*19.43*/{_display_(Seq[Any](format.raw/*19.44*/("""
							<span class="label label-info" style="background-color:rgba(77, 106, 174, 0.85);">"""),_display_(Seq[Any](/*20.91*/s)),format.raw/*20.92*/("""</span>
						""")))})),format.raw/*21.8*/("""
						<br>
						<i style="font-size:12px;">"""),_display_(Seq[Any](/*23.35*/ArticleResult/*23.48*/.getArticle().getSummary())),format.raw/*23.74*/("""</i>
					</td>
				</tr>
			""")))})),format.raw/*26.5*/("""
		</table>
		
	""")))})),format.raw/*29.3*/("""
""")))})),format.raw/*30.2*/("""
"""))}
    }
    
    def render(articleResults:List[Message],searchQuery:String): play.api.templates.Html = apply(articleResults,searchQuery)
    
    def f:((List[Message],String) => play.api.templates.Html) = (articleResults,searchQuery) => apply(articleResults,searchQuery)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jun 05 20:12:06 WEST 2013
                    SOURCE: /Users/tiagoaap/Projects/newslserver/app/views/search_results.scala.html
                    HASH: e932a9e07ef74d10f447a565f59bf807a14ea816
                    MATRIX: 746->1|875->53|912->55|950->85|989->87|1050->114|1082->138|1120->139|1227->211|1259->222|1306->239|1346->245|1378->269|1416->270|1545->364|1578->375|1635->402|1673->405|1736->459|1775->460|1889->539|1941->575|1981->577|2082->642|2104->655|2147->676|2210->703|2233->716|2280->740|2369->793|2391->806|2445->838|2498->854|2521->867|2569->892|2612->898|2635->911|2683->936|2820->1037|2843->1050|2881->1065|2937->1086|2988->1121|3027->1122|3154->1213|3177->1214|3223->1229|3305->1275|3327->1288|3375->1314|3436->1344|3484->1361|3517->1363
                    LINES: 26->1|29->1|29->1|29->1|29->1|31->3|31->3|31->3|32->4|32->4|33->5|34->6|34->6|34->6|35->7|35->7|37->9|38->10|38->10|38->10|40->12|40->12|40->12|44->16|44->16|44->16|44->16|44->16|44->16|45->17|45->17|45->17|45->17|45->17|45->17|45->17|45->17|45->17|46->18|46->18|46->18|47->19|47->19|47->19|48->20|48->20|49->21|51->23|51->23|51->23|54->26|57->29|58->30
                    -- GENERATED --
                */
            