
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object main extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Html,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(title: String)(content: Html):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.32*/("""
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>NewsL - """),_display_(Seq[Any](/*6.17*/title)),format.raw/*6.22*/("""</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<script src=""""),_display_(Seq[Any](/*12.15*/routes/*12.21*/.Assets.at("javascripts/jquery.js"))),format.raw/*12.56*/("""" type="text/javascript"></script>
<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*13.46*/routes/*13.52*/.Assets.at("stylesheets/bootstrap.css"))),format.raw/*13.91*/("""">
<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*14.46*/routes/*14.52*/.Assets.at("stylesheets/main.css"))),format.raw/*14.86*/("""">
<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*15.46*/routes/*15.52*/.Assets.at("stylesheets/datepicker.css"))),format.raw/*15.92*/("""">
<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*16.46*/routes/*16.52*/.Assets.at("stylesheets/main.css"))),format.raw/*16.86*/("""">
<script src=""""),_display_(Seq[Any](/*17.15*/routes/*17.21*/.Assets.at("javascripts/bootstrap-modal.js"))),format.raw/*17.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*18.15*/routes/*18.21*/.Assets.at("javascripts/bootstrap-datepicker.js"))),format.raw/*18.70*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*19.15*/routes/*19.21*/.Assets.at("javascripts/bootstrap-dropdown.js"))),format.raw/*19.68*/("""" type="text/javascript"></script>




<!--  <script src="assets/javascripts/bootstrap-transition.js"></script>
<script src="assets/javascripts/bootstrap-alert.js"></script>

<script src="assets/javascripts/bootstrap-dropdown.js"></script>
<script src="assets/javascripts/bootstrap-scrollspy.js"></script>
<script src="assets/javascripts/bootstrap-tab.js"></script>
<script src="assets/javascripts/bootstrap-tooltip.js"></script>
<script src="assets/javascripts/bootstrap-popover.js"></script>
<script src="assets/javascripts/bootstrap-button.js"></script>
<script src="assets/javascripts/bootstrap-collapse.js"></script>
<script src="assets/javascripts/bootstrap-carousel.js"></script>
<script src="assets/javascripts/bootstrap-typeahead.js"></script>-->

</head>
<style type="text/css">
body """),format.raw/*39.6*/("""{"""),format.raw/*39.7*/("""
	padding-top: 20px;
	padding-bottom: 20px;
"""),format.raw/*42.1*/("""}"""),format.raw/*42.2*/("""

Custom container 
      .container """),format.raw/*45.18*/("""{"""),format.raw/*45.19*/("""
	margin: 0 auto;
	max-width: 1000px;
"""),format.raw/*48.1*/("""}"""),format.raw/*48.2*/("""

.container>hr """),format.raw/*50.15*/("""{"""),format.raw/*50.16*/("""
	margin: 60px 0;
"""),format.raw/*52.1*/("""}"""),format.raw/*52.2*/("""

/* Main marketing message and sign up button */
.jumbotron """),format.raw/*55.12*/("""{"""),format.raw/*55.13*/("""
	margin: 45px 0;
	text-align: center;
"""),format.raw/*58.1*/("""}"""),format.raw/*58.2*/("""

.jumbotron h1 """),format.raw/*60.15*/("""{"""),format.raw/*60.16*/("""
	font-size: 40px;
	line-height: 1;
"""),format.raw/*63.1*/("""}"""),format.raw/*63.2*/("""

.jumbotron .lead """),format.raw/*65.18*/("""{"""),format.raw/*65.19*/("""
	font-size: 24px;
	line-height: 1.25;
"""),format.raw/*68.1*/("""}"""),format.raw/*68.2*/("""

.jumbotron .btn """),format.raw/*70.17*/("""{"""),format.raw/*70.18*/("""
	font-size: 21px;
	padding: 14px 24px;
"""),format.raw/*73.1*/("""}"""),format.raw/*73.2*/("""

/* Supporting marketing content */
.marketing """),format.raw/*76.12*/("""{"""),format.raw/*76.13*/("""
	margin: 60px 0;
"""),format.raw/*78.1*/("""}"""),format.raw/*78.2*/("""

.marketing p+h4 """),format.raw/*80.17*/("""{"""),format.raw/*80.18*/("""
	margin-top: 28px;
"""),format.raw/*82.1*/("""}"""),format.raw/*82.2*/("""
</style>

<body>

	<div class="container">

		<div class="masthead">
			<!-- <h3 class="muted" style="text-align: center;">NewsL</h3> -->
			<div class="navbar">
				<div class="navbar-inner" style="height: 0px;">
					<div class="container">
						<ul class="nav">
							<li><a href="/index">Início</a></li>
							<li><a href="/latestnews">Notícias</a></li>
							<li>
								<form action=""""),_display_(Seq[Any](/*98.24*/routes/*98.30*/.Application.search())),format.raw/*98.51*/("""" method="GET">
									<input list="list" id="search" name="search" type="text"
										autocomplete="off" class="search-query"
										style="margin-left: 70px; margin-top: 15px; width: 200px;width:200px;"
										placeholder="Escreva para pesquisar...">
								</form>
							</li>
							<li class="dropdown" style="float:right;margin-right:100px;">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">"""),_display_(Seq[Any](/*106.69*/session/*106.76*/.get("name"))),format.raw/*106.88*/("""
								<ul class="dropdown-menu">
									<li><a href="#">Favoritos</a></li>
									<li><a href="#">Alterar Definições</a></li>
									<li><a href="#">Terminar Sessão</a></li>
								</ul>
							</li>

							<datalist id="list"></datalist>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="contentContainer"">"""),_display_(Seq[Any](/*120.32*/content)),format.raw/*120.39*/("""</div>

		
	</div>



</body>

<footer style="margin-left:auto;margin-right:auto; width:940px;">
	<hr>
	<p >&copy; NewsL 2013 - Created by João Martins & Tiago Pereira</p>

</footer>
<script>
$("#search").focus(function()"""),format.raw/*135.30*/("""{"""),format.raw/*135.31*/("""
	$(this).width(300);
"""),format.raw/*137.1*/("""}"""),format.raw/*137.2*/(""");
$("#search").focusout(function()"""),format.raw/*138.33*/("""{"""),format.raw/*138.34*/("""
	$(this).width(200);
"""),format.raw/*140.1*/("""}"""),format.raw/*140.2*/(""");
/*
var search;
$("#search").keyup(function() """),format.raw/*143.31*/("""{"""),format.raw/*143.32*/("""
	if($(this).val() === "")return;
	if ($(this).val() != search) """),format.raw/*145.31*/("""{"""),format.raw/*145.32*/("""
		search = $(this).val();
		console.log(search);
		$.post('/autocomplete', """),format.raw/*148.27*/("""{"""),format.raw/*148.28*/("""'search' : search"""),format.raw/*148.45*/("""}"""),format.raw/*148.46*/(""", function(data) """),format.raw/*148.63*/("""{"""),format.raw/*148.64*/("""
			//window.alert(data[0]);
			
			if(data.length) """),format.raw/*151.20*/("""{"""),format.raw/*151.21*/("""
				var dataList = $("#list");
				dataList.empty();
				for(var i=0, len=data.length; i<len; i++) """),format.raw/*154.47*/("""{"""),format.raw/*154.48*/("""
					console.log("devolveu:"+data[i]);
					var opt = $("<option></option>").attr("value", data[i]);
					dataList.append(opt);
					
				"""),format.raw/*159.5*/("""}"""),format.raw/*159.6*/("""
			"""),format.raw/*160.4*/("""}"""),format.raw/*160.5*/("""
		"""),format.raw/*161.3*/("""}"""),format.raw/*161.4*/(""");
		return false;
	"""),format.raw/*163.2*/("""}"""),format.raw/*163.3*/("""
"""),format.raw/*164.1*/("""}"""),format.raw/*164.2*/(""").keyup();
*/
</script>
</html>

"""))}
    }
    
    def render(title:String,content:Html): play.api.templates.Html = apply(title)(content)
    
    def f:((String) => (Html) => play.api.templates.Html) = (title) => (content) => apply(title)(content)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jun 05 20:12:06 WEST 2013
                    SOURCE: /Users/tiagoaap/Projects/newslserver/app/views/main.scala.html
                    HASH: 42e53c58aa6ca99f0ba67f5f2e65ff1ad55256c3
                    MATRIX: 727->1|834->31|949->111|975->116|1194->299|1209->305|1266->340|1382->420|1397->426|1458->465|1542->513|1557->519|1613->553|1697->601|1712->607|1774->647|1858->695|1873->701|1929->735|1982->752|1997->758|2063->802|2148->851|2163->857|2234->906|2319->955|2334->961|2403->1008|3224->1802|3252->1803|3323->1847|3351->1848|3416->1885|3445->1886|3510->1924|3538->1925|3582->1941|3611->1942|3656->1960|3684->1961|3773->2022|3802->2023|3868->2062|3896->2063|3940->2079|3969->2080|4032->2116|4060->2117|4107->2136|4136->2137|4202->2176|4230->2177|4276->2195|4305->2196|4372->2236|4400->2237|4476->2285|4505->2286|4550->2304|4578->2305|4624->2323|4653->2324|4700->2344|4728->2345|5161->2742|5176->2748|5219->2769|5686->3199|5703->3206|5738->3218|6110->3553|6140->3560|6390->3781|6420->3782|6470->3804|6499->3805|6563->3840|6593->3841|6643->3863|6672->3864|6749->3912|6779->3913|6872->3977|6902->3978|7007->4054|7037->4055|7083->4072|7113->4073|7159->4090|7189->4091|7270->4143|7300->4144|7429->4244|7459->4245|7626->4384|7655->4385|7687->4389|7716->4390|7747->4393|7776->4394|7824->4414|7853->4415|7882->4416|7911->4417
                    LINES: 26->1|29->1|34->6|34->6|40->12|40->12|40->12|41->13|41->13|41->13|42->14|42->14|42->14|43->15|43->15|43->15|44->16|44->16|44->16|45->17|45->17|45->17|46->18|46->18|46->18|47->19|47->19|47->19|67->39|67->39|70->42|70->42|73->45|73->45|76->48|76->48|78->50|78->50|80->52|80->52|83->55|83->55|86->58|86->58|88->60|88->60|91->63|91->63|93->65|93->65|96->68|96->68|98->70|98->70|101->73|101->73|104->76|104->76|106->78|106->78|108->80|108->80|110->82|110->82|126->98|126->98|126->98|134->106|134->106|134->106|148->120|148->120|163->135|163->135|165->137|165->137|166->138|166->138|168->140|168->140|171->143|171->143|173->145|173->145|176->148|176->148|176->148|176->148|176->148|176->148|179->151|179->151|182->154|182->154|187->159|187->159|188->160|188->160|189->161|189->161|191->163|191->163|192->164|192->164
                    -- GENERATED --
                */
            