// @SOURCE:/Users/tiagoaap/Projects/newslserver/conf/routes
// @HASH:dfdce394c15bc38717386d6812b8049c3f640897
// @DATE:Wed Jun 05 20:12:03 WEST 2013


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix  
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" } 


// @LINE:11
private[this] lazy val controllers_Application_compile0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("compile"))))
        

// @LINE:12
private[this] lazy val controllers_Application_indexing1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("indexing"))))
        

// @LINE:16
private[this] lazy val controllers_Application_index2 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("index"))))
        

// @LINE:17
private[this] lazy val controllers_Application_latestNews3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("latestnews"))))
        

// @LINE:18
private[this] lazy val controllers_Application_index4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:19
private[this] lazy val controllers_Application_search5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("search"))))
        

// @LINE:22
private[this] lazy val controllers_Application_article6 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("article/"),DynamicPart("articleId", """[^/]+"""))))
        

// @LINE:26
private[this] lazy val controllers_Application_loginFacebook7 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("loginfacebook"))))
        

// @LINE:27
private[this] lazy val controllers_Application_autocomplete8 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("autocomplete"))))
        

// @LINE:28
private[this] lazy val controllers_Application_registerApp9 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("registerApp"))))
        

// @LINE:29
private[this] lazy val controllers_Application_login10 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:30
private[this] lazy val controllers_Application_searchByCategory11 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("searchbycategory"))))
        

// @LINE:33
private[this] lazy val controllers_Assets_at12 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+"""))))
        
def documentation = List(("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """compile""","""controllers.Application.compile()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """indexing""","""controllers.Application.indexing()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """index""","""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """latestnews""","""controllers.Application.latestNews()"""),("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """search""","""controllers.Application.search()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """article/$articleId<[^/]+>""","""controllers.Application.article(articleId:Int)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """loginfacebook""","""controllers.Application.loginFacebook()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """autocomplete""","""controllers.Application.autocomplete()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """registerApp""","""controllers.Application.registerApp()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.login()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """searchbycategory""","""controllers.Application.searchByCategory()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
       
    
def routes:PartialFunction[RequestHeader,Handler] = {        

// @LINE:11
case controllers_Application_compile0(params) => {
   call { 
        invokeHandler(controllers.Application.compile(), HandlerDef(this, "controllers.Application", "compile", Nil,"GET", """GET	/scrapingexpresso                           	controllers.Expresso.index()
GET	/scrapingdn                           			controllers.DiarioNoticias.index()
GET	/scraping										controllers.Application.scraping()""", Routes.prefix + """compile"""))
   }
}
        

// @LINE:12
case controllers_Application_indexing1(params) => {
   call { 
        invokeHandler(controllers.Application.indexing(), HandlerDef(this, "controllers.Application", "indexing", Nil,"GET", """""", Routes.prefix + """indexing"""))
   }
}
        

// @LINE:16
case controllers_Application_index2(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """WebSiteStuff""", Routes.prefix + """index"""))
   }
}
        

// @LINE:17
case controllers_Application_latestNews3(params) => {
   call { 
        invokeHandler(controllers.Application.latestNews(), HandlerDef(this, "controllers.Application", "latestNews", Nil,"GET", """""", Routes.prefix + """latestnews"""))
   }
}
        

// @LINE:18
case controllers_Application_index4(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """""", Routes.prefix + """"""))
   }
}
        

// @LINE:19
case controllers_Application_search5(params) => {
   call { 
        invokeHandler(controllers.Application.search(), HandlerDef(this, "controllers.Application", "search", Nil,"GET", """""", Routes.prefix + """search"""))
   }
}
        

// @LINE:22
case controllers_Application_article6(params) => {
   call(params.fromPath[Int]("articleId", None)) { (articleId) =>
        invokeHandler(controllers.Application.article(articleId), HandlerDef(this, "controllers.Application", "article", Seq(classOf[Int]),"GET", """""", Routes.prefix + """article/$articleId<[^/]+>"""))
   }
}
        

// @LINE:26
case controllers_Application_loginFacebook7(params) => {
   call { 
        invokeHandler(controllers.Application.loginFacebook(), HandlerDef(this, "controllers.Application", "loginFacebook", Nil,"POST", """Requests on the fly""", Routes.prefix + """loginfacebook"""))
   }
}
        

// @LINE:27
case controllers_Application_autocomplete8(params) => {
   call { 
        invokeHandler(controllers.Application.autocomplete(), HandlerDef(this, "controllers.Application", "autocomplete", Nil,"POST", """""", Routes.prefix + """autocomplete"""))
   }
}
        

// @LINE:28
case controllers_Application_registerApp9(params) => {
   call { 
        invokeHandler(controllers.Application.registerApp(), HandlerDef(this, "controllers.Application", "registerApp", Nil,"POST", """""", Routes.prefix + """registerApp"""))
   }
}
        

// @LINE:29
case controllers_Application_login10(params) => {
   call { 
        invokeHandler(controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Nil,"POST", """""", Routes.prefix + """login"""))
   }
}
        

// @LINE:30
case controllers_Application_searchByCategory11(params) => {
   call { 
        invokeHandler(controllers.Application.searchByCategory(), HandlerDef(this, "controllers.Application", "searchByCategory", Nil,"POST", """""", Routes.prefix + """searchbycategory"""))
   }
}
        

// @LINE:33
case controllers_Assets_at12(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}
    
}
        