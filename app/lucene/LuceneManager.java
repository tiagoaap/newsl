package lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import models.Article;
import models.Entidade;
import models.Message;
import models.Snipet;
import models.Topic;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.DateTools.Resolution;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;
import org.joda.time.DateTime;

import utils.POSTaggingManager;
import utils.SentenceDetectorManager;
import utils.TokenizerManager;
import utils.TopicModelManager;
import utils.WikiEntityManager;



public class LuceneManager {

	public static IndexWriter indexWriter = null;
	public static FSDirectory index = null;
	public static StandardAnalyzer analyzer = null;
	public static IndexWriterConfig config = null;
	public static String indexLocation = "/opt/Lucene";
	//public static String indexLocation = "/Users/tiagoaap/Lucene";
	//public static String indexLocation = "/Users/JoaoMartins/Lucene";
	//public static String indexLocation = "C:/luceneindex";
	SentenceDetectorManager sdm = new SentenceDetectorManager();
	TokenizerManager tm = new TokenizerManager();
	POSTaggingManager ptm = new POSTaggingManager();
	TopicModelManager tmm = new TopicModelManager();


	public LuceneManager(){}

	public void indexArticles(){

		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		//index = new RAMDirectory();

		config = new IndexWriterConfig(Version.LUCENE_41, analyzer);

		try {

			indexWriter = new IndexWriter(index, config);
			documentWorker();
			indexWriter.close();
		} catch (IOException e) {
			System.out.println("Error indexing articles!");
		}

	}

	public void documentWorker(){

		List<Article> list = Article.find.all();

		try {
			System.out.println("Making all topics...");
			tmm.trainModel(list);
		} catch (Exception e1) {
			System.out.println("[PROBLEM]: Making topics... :(");
		}

		int count = 0;

		try {
			for (Article a : list) {
				if(!a.isIndexed()){
					String article = a.getTitle()+".\t"+a.getSummary()+"\t"+a.getBody()+"\n";    
					String sentences[]=sdm.getSentences(article);
					String tokens="";
					ArrayList<String> termsArray= new ArrayList<String>();
					ArrayList<String> termsArrayAux= new ArrayList<String>();

					ArrayList<String> topics = new ArrayList<String>();
					if(!a.getBody().equals(" ")){
						topics= tmm.getTopics(article, a.getId());
					}

					for(String s : topics){
						System.out.println("Topics: "+s);
					}

					for(String s : sentences){
						tokens+=tm.getTokens(s);
						termsArrayAux=ptm.makeTagging(s);

						for(String tAux : termsArrayAux){
							boolean exists=false;
							for(String t : termsArray){
								if(t.equals(tAux)){
									exists=true;
									break;
								}
							}
							if(!exists) termsArray.add(tAux);
						}
					}

					String terms = termsArray.get(0);
					for(int i = 1; i<termsArray.size(); i++){
						terms+=";"+termsArray.get(i);
					}

					Map<String,String[]> hash = WikiEntityManager.wikiextraction(terms);
					
					addDocument(a, tokens, terms, topics, hash);

					a.setIndexed(true);
					a.save();
					if(count>2){
						break;
					}
					count++;
				}
			}

			indexWriter.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public static void addDocument(Article a, String tokens, String terms, ArrayList<String> list, Map<String,String[]> hash){
		try {
			Document doc = null;
			doc = new Document();
			doc.add(new IntField("id", a.getId(), Field.Store.YES));
			doc.add(new TextField("title", a.getTitle(), Field.Store.YES));
			if(a.getSummary()!=null)
				doc.add(new TextField("summary", a.getSummary(), Field.Store.YES));
			else
				doc.add(new TextField("summary", " ", Field.Store.YES));
			doc.add(new TextField("author", a.getAuthor(), Field.Store.YES));
			doc.add(new TextField("date", buildDate(a.getDate().getMillis()), Field.Store.YES));
			if(a.getBody()!=null)
				doc.add(new TextField("body", a.getBody(), Field.Store.YES));
			else
				doc.add(new TextField("body", " ", Field.Store.YES));
			if(a.getImage()!=null){
				doc.add(new TextField("image", a.getImage(), Field.Store.YES));
			}else doc.add(new TextField("image", " ", Field.Store.YES));

			doc.add(new TextField("category", a.getCategory(), Field.Store.YES));
			doc.add(new TextField("url", a.getURL(), Field.Store.YES));
			doc.add(new TextField("source", a.getSource(), Field.Store.YES));
			doc.add(new TextField("terms", terms, Field.Store.YES));
			for(String s : list){
				//System.out.println("vou indexar o tópico: "+s);
				doc.add(new TextField("topics", s, Field.Store.YES));
			}
			for (Map.Entry<String, String[]> entry : hash.entrySet()) {
				String termIndex = "";
				String key = entry.getKey();
				termIndex +=key+":";
				String []aux = entry.getValue();
				for(String s: aux){
					termIndex +=" "+s;
				}
				doc.add(new TextField("entities", termIndex, Field.Store.YES));
			}

			doc.add(new TextField("views", ""+0, Field.Store.YES));

			indexWriter.addDocument(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Message getArticleById(int id){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try{
			@SuppressWarnings("deprecation")
			IndexReader reader = IndexReader.open(index);

			IndexSearcher searcher = new IndexSearcher(reader);


			Query q2 = NumericRangeQuery.newIntRange("id", 1, id, id, true, true);
			TopDocs td2 = searcher.search(q2, 1);
			ScoreDoc[] docs = td2.scoreDocs;

			System.out.println("Found "+docs.length+" "+id);

			for(int i=0;i<docs.length;++i) {
				int docId = docs[i].doc;
				Document d = searcher.doc(docId);
				int idArticle = Integer.parseInt(d.get("id"));
				if(idArticle==id){

					Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"), d.get("image"));
					//System.out.println(a.toString());
					//System.out.println("Article views: "+d.get("views"));

					Document updateDoc = new Document();

					List<Topic> topics = new ArrayList<Topic>();
					List<Entidade> entities = new ArrayList<Entidade>();
					

					for(IndexableField f : d.getFields()){
						if(f.name().equals("id")){
							updateDoc.add(new IntField("id", Integer.parseInt(f.stringValue()), Field.Store.YES));
						}
						else if(f.name().equals("title")){
							updateDoc.add(new TextField("title", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("summary")){
							updateDoc.add(new TextField("summary", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("author")){
							updateDoc.add(new TextField("author", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("date")){
							updateDoc.add(new TextField("date", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("body")){
							updateDoc.add(new TextField("body", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("image")){
							updateDoc.add(new TextField("image", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("category")){
							updateDoc.add(new TextField("category", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("url")){
							updateDoc.add(new TextField("url", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("source")){
							updateDoc.add(new TextField("source", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("source")){
							updateDoc.add(new TextField("source", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("terms")){
							updateDoc.add(new TextField("terms", f.stringValue(), Field.Store.YES));
						}
						else if(f.name().equals("topics")){
							updateDoc.add(new TextField("topics", f.stringValue(), Field.Store.YES));
							Topic t = new Topic(f.stringValue());
							//t.save();
							topics.add(t);
						}
						else if(f.name().equals("entities")){
							updateDoc.add(new TextField("entities", f.stringValue(), Field.Store.YES));
							Entidade e = new Entidade(f.stringValue());
							//e.save();
							entities.add(e);
						}
						else if(f.name().equals("views")){
							int views = 0;
							try{
								views = Integer.parseInt(f.stringValue());
							} catch (Exception e){
							}
							//System.out.println("Visitas: "+views);
							views++;
							//System.out.println("Pós update Visitas: "+views);
							updateDoc.add(new TextField("views", ""+views, Field.Store.YES));
						}
					}

					config = new IndexWriterConfig(Version.LUCENE_41, analyzer);

					try {
						indexWriter = new IndexWriter(index, config);
						indexWriter.deleteDocuments(q2);

						//System.out.println("Views: "+updateDoc.get("views"));
						//System.out.println("TOPICS do menino: "+updateDoc.get("topics"));
						indexWriter.addDocument(updateDoc);
						indexWriter.close();
					} catch (IOException e) {
						//System.out.println("Error updating articles!");
					}



					index.close();
					
					Message msg = new Message(a, 0, new String[10], "", Integer.parseInt(updateDoc.get("views")), topics, entities);
					
					return msg;
				}
			}

		} catch (Exception e){
			e.printStackTrace();
		}

		index.close();
		return null;
	}

	public static String buildDate(long time) {
		//System.out.println("TIME: "+time);
		return DateTools.dateToString(new Date(time), Resolution.SECOND);
	}

	public static int padRemove(String c) {
		//System.out.println("c: "+c);
		if(c.charAt(0)=='0'){
			return Integer.parseInt(c.substring(1));
		}

		else{
			int res = Integer.parseInt(c);
			return res;
		}

	}

	public int verifyYear(String year){

		int y;
		Calendar c = Calendar.getInstance();
		try {
			y = Integer.parseInt(year);
			if(c.get(Calendar.YEAR)>y && y<2011){
				return -1;
			}
		} catch (Exception e) {
			return -1;
		}

		return y;
	}


	public String verifyDate(String month){

		if(month.equalsIgnoreCase("Janeiro")){
			return "01";
		}
		else if(month.equalsIgnoreCase("Fevereiro")){
			return "02";
		}
		else if(month.equalsIgnoreCase("Março")){
			return "03";
		}
		else if(month.equalsIgnoreCase("Abril")){
			return "04";
		}
		else if(month.equalsIgnoreCase("Maio")){
			return "05";
		}
		else if(month.equalsIgnoreCase("Junho")){
			return "06";
		}
		else if(month.equalsIgnoreCase("Julho")){
			return "07";
		}
		else if(month.equalsIgnoreCase("Agosto")){
			return "08";
		}
		else if(month.equalsIgnoreCase("Setembro")){
			return "09";
		}
		else if(month.equalsIgnoreCase("Outubro")){
			return "10";
		}
		else if(month.equalsIgnoreCase("Novembro")){
			return "11";
		}
		else if(month.equalsIgnoreCase("Dezembro")){
			return "12";
		}

		return "nodate";
	}


	public List<Message> luceneSearchByCategory(String search){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try{
			@SuppressWarnings("deprecation")
			IndexReader reader = IndexReader.open(index);

			IndexSearcher searcher = new IndexSearcher(reader);
			String fields2[] = {"category"};

			List<Message> articlesAux = new ArrayList<Message>();
			Query q = new MultiFieldQueryParser(Version.LUCENE_41, fields2, analyzer).parse(search);



			TopScoreDocCollector collector = TopScoreDocCollector.create(30000, true);
			searcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
			for(int i=0;i<hits.length;++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"),d.get("image"));
				Message m = new Message(a, 1, search.split("-"), "",Integer.parseInt(d.get("views")));
				articlesAux.add(m);
				//System.out.println("[id: "+d.get("id")+"] - URL: "+d.get("title"));
			}


			index.close();
			return getMessagesByOrder(articlesAux);	

		} catch (Exception e){
			index.close();
			e.printStackTrace();
		}

		return new ArrayList<Message>();
	}
	
	
	public List<Message> luceneSearchSuggestions(String search, String category){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try{
			@SuppressWarnings("deprecation")
			IndexReader reader = IndexReader.open(index);

			IndexSearcher searcher = new IndexSearcher(reader);
			String fields2[] = {"category"};

			List<List<Article>> listContainer = new ArrayList<List<Article>>();
			List<Article> categoryList  = new ArrayList<Article>();
			Query q = new MultiFieldQueryParser(Version.LUCENE_41, fields2, analyzer).parse(category);



			TopScoreDocCollector collector = TopScoreDocCollector.create(30000, true);
			searcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
			for(int i=0;i<hits.length;++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"),d.get("image"));
				categoryList.add(a);
			}

			listContainer.add(categoryList);
			
			
			String fields[] = {"topics"};
			
			search=search.replaceAll(" ", "-");
			
			String[] topicsToSearch=search.split("-");
			
			
			
			for(String s : topicsToSearch){
				List<Article> articlesAux = new ArrayList<Article>();
				if(!s.equals(""))
					q = new MultiFieldQueryParser(Version.LUCENE_41, fields, analyzer).parse(s);

				searcher = new IndexSearcher(reader);
				collector = TopScoreDocCollector.create(30000, true);
				searcher.search(q, collector);
				hits = collector.topDocs().scoreDocs;
				for(int i=0;i<hits.length;++i) {
					int docId = hits[i].doc;
					Document d = searcher.doc(docId);
					Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"), d.get("image"));
					articlesAux.add(a);

				}
				listContainer.add(articlesAux);
			}


			List<Message> listToReturn = new ArrayList<Message>();
			
			for(List<Article> list:listContainer){
				for(Article a:list){
					String str = "";
					int count = 0;
					
					for(List<Article> listInside:listContainer){
						for(Article aToCompare:listInside){
							if(aToCompare.getId()==a.getId()){
								count++;
								break;
							}
						}
					}
					
					if(count>10){
						if(a.getCategory().equalsIgnoreCase(category)){
							Message m = new Message(a, count, str.split("-"), null);
							updateMessage(listToReturn, m);
						}
					}

				}
			}
			
			index.close();

			return getMessagesByOrder(listToReturn);
			
		} catch (Exception e){
			index.close();
			e.printStackTrace();
		}

		return new ArrayList<Message>();
	}



	@SuppressWarnings({"deprecation"})
	public List<Message> luceneSearchLastDay(String search){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//System.out.println("Performing luceneSearchLastDay method");

		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);
		//System.out.println("Yesterday's date = "+ yesterday.getTime());

		Calendar today = Calendar.getInstance();
		//System.out.println("Todays date = "+ today.getTime());


		String date1=buildDate(yesterday.getTimeInMillis());
		String date2=buildDate(today.getTimeInMillis());


		//System.out.println("datas construídas... proceder à query...");

		IndexReader reader = null;
		try {
			reader = IndexReader.open(index);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		IndexSearcher searcher = new IndexSearcher(reader);

		BytesRef r1 = new BytesRef(date1.getBytes());
		BytesRef r2 = new BytesRef(date2.getBytes());
		TermRangeQuery q2 = new TermRangeQuery("date",r1, r2, true, true);
		TopDocs td2 = null;

		List<Article> articlesDate = new ArrayList<Article>();

		try {
			td2 = searcher.search(q2, 300);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ScoreDoc[] docs = td2.scoreDocs;
		for(int i=0;i<docs.length;++i) {
			int docId = docs[i].doc;
			Document d = null;
			try {
				d = searcher.doc(docId);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("DOC ID DATE: "+d.get("id"));
			//System.out.println("DOC TITLE DATE: "+d.get("title"));
			Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"),d.get("image"));
			//System.out.println("ARTICLE: "+a.toString());
			articlesDate.add(a);
		}

		List<List<Article>> listContainer = new ArrayList<List<Article>>();

		listContainer.add(articlesDate);



		try{
			String fields2[] = {"category"};

			List<Article> articlesAux = new ArrayList<Article>();
			Query q = new MultiFieldQueryParser(Version.LUCENE_41, fields2, analyzer).parse(search);
			//System.out.println("I'm searching for "+search);


			TopScoreDocCollector collector = TopScoreDocCollector.create(1000, true);
			searcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
			for(int i=0;i<hits.length;++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"),d.get("image"));
				articlesAux.add(a);
				//System.out.println("[id: "+d.get("id")+"] - URL: "+d.get("title"));
			}
			listContainer.add(articlesAux);


			//System.out.println("ARTIGOS!!");
			int nLists = listContainer.size();



			List<Message> listToReturn = new ArrayList<Message>();


			//System.out.println("Number of lists: "+nLists);
			for(List<Article> list:listContainer){
				for(Article a:list){
					String str = "";
					int count = 0;
					int ind = 0;
					//System.out.println("INDEXXX: "+ind);
					for(List<Article> listInside:listContainer){


						for(Article aToCompare:listInside){
							//System.out.println(a.getId() + " - "+ aToCompare.getId());
							if(aToCompare.getId()==a.getId()){
								count++;
								break;
							}
						}
						ind++;

					}

					//System.out.println("Adicionar o artigo: "+a.toString());

					//System.out.println("str: "+str);

					if(count==2){
						Message m = new Message(a, count, str.split("-"));
						updateMessage(listToReturn, m);
					}

					//System.out.println("count: "+count);
					//System.out.println("index:" +ind);
					//System.out.println("str: "+str );
					//System.out.println("<==========END=========>\n");

				}
			}

			index.close();

			return getMessagesByOrder(listToReturn);

		} catch (Exception e){

		}
		return null;
	}



	public List<Message> luceneSearchTopLastDay(){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//System.out.println("Performing luceneSearchTopLastDay method");

		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);
		//System.out.println("Yesterday's date = "+ yesterday.getTime());

		Calendar today = Calendar.getInstance();
		//System.out.println("Todays date = "+ today.getTime());


		String date1=buildDate(yesterday.getTimeInMillis());
		String date2=buildDate(today.getTimeInMillis());


		//System.out.println("datas construídas... proceder à query...");

		IndexReader reader = null;
		try {
			reader = IndexReader.open(index);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		IndexSearcher searcher = new IndexSearcher(reader);

		BytesRef r1 = new BytesRef(date1.getBytes());
		BytesRef r2 = new BytesRef(date2.getBytes());
		TermRangeQuery q2 = new TermRangeQuery("date",r1, r2, true, true);
		TopDocs td2 = null;

		
		List<Message> messages = new ArrayList<Message>();
		try {
			td2 = searcher.search(q2, 200);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ScoreDoc[] docs = td2.scoreDocs;
		for(int i=0;i<docs.length;++i) {
			int docId = docs[i].doc;
			Document d = null;
			try {
				d = searcher.doc(docId);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("DOC ID DATE: "+d.get("id"));
			//System.out.println("DOC TITLE DATE: "+d.get("title"));
			Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"),d.get("image"));
			//System.out.println("ARTICLE: "+a.toString());
			
			messages.add(new Message(a, 0, new String[10], "", Integer.parseInt(d.get("views"))));
		}
		
		return getMessagesByOrder(messages);
	}
	
	public List<Message> luceneSearchTopLastWeek(){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//System.out.println("Performing luceneSearchTopLastDay method");

		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -6);
		//System.out.println("Yesterday's date = "+ yesterday.getTime());

		Calendar today = Calendar.getInstance();
		//System.out.println("Todays date = "+ today.getTime());


		String date1=buildDate(yesterday.getTimeInMillis());
		String date2=buildDate(today.getTimeInMillis());


		//System.out.println("datas construídas... proceder à query...");

		IndexReader reader = null;
		try {
			reader = IndexReader.open(index);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		IndexSearcher searcher = new IndexSearcher(reader);

		BytesRef r1 = new BytesRef(date1.getBytes());
		BytesRef r2 = new BytesRef(date2.getBytes());
		TermRangeQuery q2 = new TermRangeQuery("date",r1, r2, true, true);
		TopDocs td2 = null;

		
		List<Message> messages = new ArrayList<Message>();
		try {
			td2 = searcher.search(q2, 1000);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ScoreDoc[] docs = td2.scoreDocs;
		for(int i=0;i<docs.length;++i) {
			int docId = docs[i].doc;
			Document d = null;
			try {
				d = searcher.doc(docId);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("DOC ID DATE: "+d.get("id"));
			//System.out.println("DOC TITLE DATE: "+d.get("title"));
			Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"),d.get("image"));
			//System.out.println("ARTICLE: "+a.toString());
			
			messages.add(new Message(a, 0, new String[10], "", Integer.parseInt(d.get("views"))));
		}
		
		return getMessagesByOrder(messages);
	}

	@SuppressWarnings("deprecation")
	public List<Message> luceneSearchRelation(String searchkey){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int hitsPerPage = 500;

		String searchTokens[]=searchkey.split(" ");

		List<String> finalTokens = new ArrayList<String>();
		List<Article> articlesFinal = new ArrayList<Article>();

		for(String s : searchTokens)
			finalTokens.add(s);


		List<List<Article>> listContainer = new ArrayList<List<Article>>();

		List<String> tokens = new ArrayList<String>();

		List<Snipet> snipets = new ArrayList<Snipet>();

		try{
			String fields2[] = {"topics", "entities", "terms","category", "title", "summary"};

			for(String s : finalTokens){
				tokens.add(s);
				List<Article> articlesAux = new ArrayList<Article>();
				Query q = new MultiFieldQueryParser(Version.LUCENE_41, fields2, analyzer).parse(s);
				//System.out.println("I'm searching for "+s);


				IndexReader reader = IndexReader.open(index);

				IndexSearcher searcher = new IndexSearcher(reader);
				TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
				searcher.search(q, collector);
				ScoreDoc[] hits = collector.topDocs().scoreDocs;
				for(int i=0;i<hits.length;++i) {
					int docId = hits[i].doc;
					Document d = searcher.doc(docId);
					Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"), d.get("image"));
					articlesAux.add(a);
					//System.out.println("[id: "+d.get("id")+"] - URL: "+d.get("title"));

					TokenStream stream = TokenSources.getTokenStream("body", d.get("body"), analyzer);
					QueryScorer scorer = new QueryScorer(q);
					Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, 60);

					Highlighter highlighter = new Highlighter(scorer);
					highlighter.setTextFragmenter(fragmenter);
					highlighter.setMaxDocCharsToAnalyze(Integer.MAX_VALUE);

					String[] fragments = highlighter.getBestFragments(stream, d.get("body"), 5);
					//System.out.println("Fragments size: "+fragments.length);
					//System.out.println("Scorer: "+scorer.getFragmentScore());
					String aux = "";
					for(String srt : fragments){
						aux+=srt+"... ";
						//System.out.println("Fragment: "+srt);	
					}

					Snipet sp = null;

					if(aux.equals("")){
						sp = new Snipet(a.getId(), "");
					}
					else{
						sp = new Snipet(a.getId(), "... "+aux.replace("<B>", "<strong>").replace("</B>", "</strong>"));
					}

					boolean insert=true;

					for(Snipet snp : snipets){
						if(snp.getArticleId()==sp.getArticleId()){
							insert=false;
						}
					}

					if(insert){
						snipets.add(sp);
					}


				}
				listContainer.add(articlesAux);
			}


			//System.out.println("ARTIGOS!!");
			int nLists = listContainer.size();



			List<Message> listToReturn = new ArrayList<Message>();

			
			
			//System.out.println("Number of lists: "+nLists);
			for(List<Article> list:listContainer){
				for(Article a:list){
					String str = "";
					int count = 0;
					int ind = 0;
					//System.out.println("INDEXXX: "+ind);
					for(List<Article> listInside:listContainer){


						for(Article aToCompare:listInside){
							if(aToCompare.getId()==a.getId()){
								count++;
								str+=tokens.get(ind)+"-";
								break;
							}
						}
						ind++;

					}
					Message m=null;
					String snipet ="";
					if(count==nLists){
						for(Snipet sp : snipets){
							if(sp.getArticleId()==a.getId())
								snipet=sp.getText();
						}
						
						m = new Message(a, count, str.split("-"), snipet);
						updateMessage(listToReturn, m);
					}

				}
			}

			//System.out.println("TOTAL DOCUMENTS: "+listToReturn.size());


			index.close();

			return getMessagesByOrder(listToReturn);

		} catch (Exception e){
			e.printStackTrace();
		}

		return null;

	}
	



	@SuppressWarnings("deprecation")
	public List<Message> luceneSearch(String searchkey){

		/*Create analyzer and define index to point to lucene directory*/
		analyzer = new StandardAnalyzer(Version.LUCENE_41);
		try {
			index = FSDirectory.open(new File(indexLocation));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int hitsPerPage = 2000;

		String searchTokens[]=searchkey.split(" ");

		List<String> finalTokens = new ArrayList<String>();
		List<Article> articlesDate = new ArrayList<Article>();
		List<Article> articlesFinal = new ArrayList<Article>();

		int month = -1;
		int year = -1;

		boolean verify = false;
		String tokenDate = "";


		for(String s : searchTokens){
			String value = verifyDate(s);
			//System.out.println("Token value: "+value);
			if(!value.equals("nodate")){
				month=Integer.parseInt(value);
				tokenDate+=s+"-";
				//System.out.println("tokenDate: "+tokenDate);
			}
			else{
				if(verifyYear(s)!=-1){
					year = Integer.parseInt(s);
					tokenDate+=s+"-";
					//System.out.println("tokenDate: "+tokenDate);
				}
				else{
					finalTokens.add(s);
				}

			}
		}

		DateTime startDate = null;
		DateTime endDate = null;

		if(month!=-1 && year!=-1){
			startDate = new DateTime(year, month, 1, 0, 0);
			if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
				endDate = new DateTime(year, month, 31, 0, 0);
			}
			else if (month==2){
				endDate = new DateTime(year, month, 28, 0, 0);
			}
			else{
				endDate = new DateTime(year, month, 30, 0, 0);
			}
		}
		else if(month!=-1){
			int actualYear = Calendar.getInstance().get(Calendar.YEAR);
			startDate = new DateTime(actualYear, month, 1, 0, 0);
			if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
				endDate = new DateTime(actualYear, month, 31, 0, 0);
			}
			else if (month==2){
				endDate = new DateTime(actualYear, month, 28, 0, 0);
			}
			else{
				endDate = new DateTime(actualYear, month, 30, 0, 0);
			}
		}

		if(startDate!=null){

			verify=true;

			//System.out.println("Data inicial: "+startDate.toString());
			//System.out.println("Data final: "+endDate.toString());

			String date1=buildDate(startDate.getMillis());
			String date2=buildDate(endDate.getMillis());

			IndexReader reader = null;
			try {
				reader = IndexReader.open(index);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			IndexSearcher searcher = new IndexSearcher(reader);

			BytesRef r1 = new BytesRef(date1.getBytes());
			BytesRef r2 = new BytesRef(date2.getBytes());
			TermRangeQuery q2 = new TermRangeQuery("date",r1, r2, true, true);
			TopDocs td2 = null;
			try {
				td2 = searcher.search(q2, hitsPerPage);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ScoreDoc[] docs = td2.scoreDocs;
			for(int i=0;i<docs.length;++i) {
				int docId = docs[i].doc;
				Document d = null;
				try {
					d = searcher.doc(docId);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("DOC ID DATE: "+d.get("id"));
				//System.out.println("DOC TITLE DATE: "+d.get("title"));
				Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"), d.get("image"));
				//System.out.println("ARTICLE: "+a.toString());
				articlesDate.add(a);
			}


		}



		if(finalTokens.isEmpty()){
			List<Message> list = new ArrayList<Message>();
			for(Article a : articlesDate){
				list.add(new Message(a, 0, tokenDate.split("-"), ""));
			}
			return list;
		}

		List<List<Article>> listContainer = new ArrayList<List<Article>>();

		listContainer.add(articlesDate);


		List<String> tokens = new ArrayList<String>();
		tokens.add(tokenDate);

		List<Snipet> snipets = new ArrayList<Snipet>();

		try{
			String fields2[] = {"topics", "entities", "terms","category", "title", "summary"};

			for(String s : finalTokens){
				tokens.add(s);
				List<Article> articlesAux = new ArrayList<Article>();
				Query q = new MultiFieldQueryParser(Version.LUCENE_41, fields2, analyzer).parse(s);
				//System.out.println("I'm searching for "+s);


				IndexReader reader = IndexReader.open(index);

				IndexSearcher searcher = new IndexSearcher(reader);
				TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
				searcher.search(q, collector);
				ScoreDoc[] hits = collector.topDocs().scoreDocs;
				for(int i=0;i<hits.length;++i) {
					int docId = hits[i].doc;
					Document d = searcher.doc(docId);
					Article a = new Article(Integer.parseInt(d.get("id")), d.get("title"), d.get("summary"), d.get("author"), new DateTime(Integer.parseInt(d.get("date").substring(0, 4)), padRemove(d.get("date").substring(4, 6)), padRemove(d.get("date").substring(6, 8)), 0, 0), d.get("body"), d.get("source"), d.get("category"), d.get("url"), d.get("image"));
					articlesAux.add(a);
					//System.out.println("[id: "+d.get("id")+"] - URL: "+d.get("title"));

					TokenStream stream = TokenSources.getTokenStream("body", d.get("body"), analyzer);
					QueryScorer scorer = new QueryScorer(q);
					Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, 60);

					Highlighter highlighter = new Highlighter(scorer);
					highlighter.setTextFragmenter(fragmenter);
					highlighter.setMaxDocCharsToAnalyze(Integer.MAX_VALUE);

					String[] fragments = highlighter.getBestFragments(stream, d.get("body"), 5);
					//System.out.println("Fragments size: "+fragments.length);
					//System.out.println("Scorer: "+scorer.getFragmentScore());
					String aux = "";
					for(String srt : fragments){
						aux+=srt+"... ";
						//System.out.println("Fragment: "+srt);	
					}

					Snipet sp = null;

					if(aux.equals("")){
						sp = new Snipet(a.getId(), "");
					}
					else{
						sp = new Snipet(a.getId(), "... "+aux.replace("<B>", "").replace("</B>", ""));
					}

					boolean insert=true;

					for(Snipet snp : snipets){
						if(snp.getArticleId()==sp.getArticleId()){
							insert=false;
						}
					}

					if(insert){
						snipets.add(sp);
					}


				}
				listContainer.add(articlesAux);
			}


			//System.out.println("ARTIGOS!!");
			int nLists = listContainer.size();



			List<Message> listToReturn = new ArrayList<Message>();


			//System.out.println("Number of lists: "+nLists);
			for(List<Article> list:listContainer){
				for(Article a:list){
					String str = "";
					int count = 0;
					int ind = 0;
					//System.out.println("INDEXXX: "+ind);
					for(List<Article> listInside:listContainer){


						for(Article aToCompare:listInside){
							//System.out.println(a.getId() + " - "+ aToCompare.getId());
							if(aToCompare.getId()==a.getId()){
								count++;
								str+=tokens.get(ind)+"-";
								break;
							}
						}
						ind++;

					}

					String snipet ="";
					for(Snipet sp : snipets){
						if(sp.getArticleId()==a.getId())
							snipet=sp.getText();
					}

					if(!snipet.equals("")){
						for(String s : finalTokens){
							snipet=snipet.replaceAll("(?i)"+s, "<strong>"+s+"</strong>");	
						}
					}
					
					Message m = new Message(a, count, str.split("-"), snipet);


					updateMessage(listToReturn, m);


				}
			}


			//System.out.println("TOTAL DOCUMENTS: "+articlesFinal.size());


			index.close();

			return getMessagesByOrder(listToReturn);

		} catch (Exception e){
			e.printStackTrace();
		}

		return null;

	}


	public List<Message> getMessagesByOrder(List<Message> messages){

		List<Message> inOrder = new ArrayList<Message>();


		for(Message m : messages){
			int i = 0;
			for(Message msg : inOrder){
				if(m.getCount()<msg.getCount()){
					i++;
				}
				else if(m.getCount()==msg.getCount()){
					if(m.getArticle().getDate().isBefore(msg.getArticle().getDate())){
						i++;
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}
			
			inOrder.add(i, m);
		}

		return inOrder;

	}


	public int updateMessage(List<Message> messages, Message m){

		for(Message msg : messages){
			if(msg.getArticle().getId()==m.getArticle().getId()){
				if(msg.getCount()<m.getCount()){
					msg.setCount(m.getCount());
					msg.setTokens(m.getTokens());
					return 1;
				}
				else if(msg.getCount()==m.getCount()){
					return 0;
				}
			}
		}

		messages.add(m);
		return 2;
	}

}


