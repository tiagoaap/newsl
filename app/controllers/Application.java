package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lucene.LuceneManager;
import models.Article;
import models.Comment;
import models.Message;
import models.Suggestion;
import models.Topic;
import models.User;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;
import utils.DiarioNoticias;
import utils.Expresso;
import utils.Password;
import java.util.Collections;
import views.html.index;
import views.html.search_cat;
import views.html.search_results;
import views.html.show_article;
import views.html.errorpage;

public class Application extends Controller {




	public static Result scraping() {
		System.out.println("Scraping");
		Expresso.index();
		DiarioNoticias.index();
		return ok("All done");
	}

	public static Result compile(){
		return ok("Compiled");
	}

	public static Result indexing(){

		LuceneManager lm = new LuceneManager();
		lm.indexArticles();

		return ok("All articles are indexed");
	}



	/**
	------------------------------
	THIS IS THE WEBSITE CONTROLLER 
	------------------------------
	 **/

	public static Result index() {
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchTopLastDay();

		List<Message> inOrder = new ArrayList<Message>();
		List<Message> toReturn = new ArrayList<Message>(); 
		for(Message m : list){
			int i = 0;
			for(Message msg : inOrder){
				if(m.getViews()<msg.getViews()){
					i++;
				}
				else{
					break;
				}
			}
			inOrder.add(i, m);
		}

		for (int i = 0; i < inOrder.size(); i++) {
			if(i==16){
				break;
			}
			else{
				toReturn.add(inOrder.get(i));
			}
		}
		List<Message> finalResults = new ArrayList<Message>();
		if(toReturn.size()>10){
			for(int k = 0;k<10;k++){
				finalResults.add(toReturn.get(k));
			}
		}else{

			for(Message k: toReturn){
				finalResults.add(k);
			}
		}	
		List<Message> toReturn2 = new ArrayList<Message>(); 
		try{
			List<Message> list2 = null;
			list2 = lm.luceneSearchTopLastWeek();

			List<Message> inOrder2 = new ArrayList<Message>();
			
			for(Message m : list2){
				int i = 0;
				for(Message msg : inOrder2){
					if(m.getViews()<msg.getViews()){
						i++;
					}
					else{
						break;
					}
				}
				inOrder2.add(i, m);
			}

			for (int i = 0; i < inOrder2.size(); i++) {
				if(i==8){
					break;
				}
				else{
					toReturn2.add(inOrder2.get(i));
				}
			}
			}catch(Exception e){
				e.printStackTrace();
				return notFound(errorpage.render(" está a tentar aceder a uma página inexistente."));
			}
		
		
		return ok(index.render("Início", getSuggestions(),finalResults, toReturn2));
	}


	public static Result clearHistory(){
		
		User u = User.find.where().eq("email", Context.current().session().get("email")).findUnique();
		for(Suggestion s :u.getSuggestions()){
			s.delete();
		}
		u.save();
		return index();
	}
	
	public static Result addComment(int id){
		
		DynamicForm data = Form.form().bindFromRequest();

		String text = data.get("commenttext");
		if(text.length()>320){
			return notFound(errorpage.render(" atingiu o limite de caractéres permitido num comentário."));
		}
		Article a = Article.findById.where().eq("id", id).findUnique();
		Comment c = new Comment(Context.current().session().get("name"), text);
		a.addComment(c);
		a.save();
		
		return redirect("/article/"+id);
	}
	
	
	public static Result search(int page){
		DynamicForm data = Form.form().bindFromRequest();
		String search = data.get("search");
		LuceneManager lm = new LuceneManager();
		List<Message> list = lm.luceneSearch(search);
		return ok(search_results.render(pagination(list, page),search,page));
	}

	
	public static Result article(int articleId){
		try{
			
		
		LuceneManager lm = new LuceneManager();
		Message article = lm.getArticleById(articleId);

		/*UPDATE SUGGESTIONS*/
		User u = User.find.where().eq("email", Context.current().session().get("email")).findUnique();
		boolean verify=true;

		if(u!=null){
			for(Suggestion s : u.getSuggestions()){
				if(s.getArticleId() == article.getArticle().getId()){
					s.updateCountViews();
					u.save();
					verify = false;
					break;
				}
			}

			if(verify){
				Suggestion s = new Suggestion(article.getArticle().getId(), article.getArticle().getCategory(), article.getTopics(), article.getEntities());
				u.addSuggestions(s);
				u.save();
			}
		}
		/*GET RELATED ARTICLES*/

		List<Message> list = lm.luceneSearchByCategory(article.getArticle().getCategory());
		List<Message> returnList = new ArrayList<Message>();

		int count =0;
		for (int i = 0; i < list.size(); i++) {
			if(count<6){
				if(list.get(i).getArticle().getId()!=articleId){
					returnList.add(list.get(i));
					count++;
				}
			}
			else break;
		}
		/*FINAL GET RELATED ARTICLES*/
		
		Article a = Article.findById.where().eq("id", article.getArticle().getId()).findUnique();
		List<Comment> array = new ArrayList<Comment>();
		for (Comment c : a.getComments()){
			int i=0;
			for(Comment c2 : array){
				if(c.getId()<c2.getId()){
					i++;
				}
			}
			array.add(i,c);
		}
		return ok(show_article.render(article,returnList,array));
		}catch(Exception e){
			return notFound(errorpage.render(" está a tentar aceder a uma página inexistente."));
		}
	}


	public static Result articleWithSug(int articleId, String query){
		try{
		List<Message> returnList = new ArrayList<Message>();

		LuceneManager lm = new LuceneManager();


		Message article = lm.getArticleById(articleId);

		/*UPDATE SUGGESTIONS*/
		User u = User.find.where().eq("email", Context.current().session().get("email")).findUnique();
		boolean verify=true;

		if(u!=null){
			for(Suggestion s : u.getSuggestions()){
				if(s.getArticleId() == article.getArticle().getId()){
					s.updateCountViews();
					u.save();
					//System.out.println("Suggestion: "+s.toString());
					verify = false;
					break;
				}
			}

			if(verify){
				Suggestion s = new Suggestion(article.getArticle().getId(), article.getArticle().getCategory(), article.getTopics(), article.getEntities());
				u.addSuggestions(s);
				u.save();
				//System.out.println("Suggestion: "+s.toString());
			}
		}
		/*FINAL UPDATE SUGGESTIONS*/

		/*GET RELATED ARTICLES*/
		List<Message> list = lm.luceneSearchRelation(query.split("-")[0]+" "+query.split("-")[1]);

		if(list.isEmpty()){
			list = lm.luceneSearchByCategory(query.split("-")[1]);
		}

		int count =0;
		for (int i = 0; i < list.size(); i++) {
			if(count<6){
				if(list.get(i).getArticle().getId()!=articleId){
					returnList.add(list.get(i));
					count++;
				}
			}
			else break;
		}

		if(returnList.isEmpty()){

			list = lm.luceneSearchByCategory(query.split("-")[1]);

			count =0;
			for (int i = 0; i < list.size(); i++) {
				if(count<6){
					if(list.get(i).getArticle().getId()!=articleId){
						returnList.add(list.get(i));
						count++;
					}
				}
				else break;
			}
		}
		/*FINAL GET RELATED ARTICLES*/

		
		Article a = Article.findById.where().eq("id", article.getArticle().getId()).findUnique();
		
		return ok(show_article.render(article, returnList, a.getComments()));
		}catch(Exception e){
			return notFound(errorpage.render(" está a tentar aceder a uma página inexistente."));
		}
	}



	public static List<Message> getSuggestions(){


		//System.out.println("SUGGESTIONS!!!");

		User u = User.find.where().eq("email", Context.current().session().get("email")).findUnique();

		if(u==null)
			return null;

		List<Suggestion> userSearchs = u.getSuggestions();


		int politics=0, economics=0, national=0, international=0, society=0, sports=0, science=0, culture=0, opinion=0;
		int total=0;

		String politicsTopics="", economicsTopics="", nationalTopics="", internationalTopics="", societyTopics="";
		String sportsTopics="", cultureTopics="", opinionTopics="", scienceTopics="";

		for(Suggestion s : userSearchs){
			if(s.getCategory().equalsIgnoreCase("política")){
				for(Topic t : s.getTopics())
					politicsTopics+=" "+t.getText();
				politics++;
			}
			else if(s.getCategory().equalsIgnoreCase("economia")){
				for(Topic t : s.getTopics())
					economicsTopics+=" "+t.getText();
				economics++;
			}
			else if(s.getCategory().equalsIgnoreCase("nacional")){
				for(Topic t : s.getTopics())
					nationalTopics+=" "+t.getText();
				national++;
			}
			else if(s.getCategory().equalsIgnoreCase("internacional")){
				for(Topic t : s.getTopics())
					internationalTopics+=" "+t.getText();
				international++;
			}
			else if(s.getCategory().equalsIgnoreCase("sociedade")){
				for(Topic t : s.getTopics())
					societyTopics+=" "+t.getText();
				society++;
			}
			else if(s.getCategory().equalsIgnoreCase("desporto")){
				for(Topic t : s.getTopics())
					sportsTopics+=" "+t.getText();
				sports++;
			}
			else if(s.getCategory().equalsIgnoreCase("ciência")){
				for(Topic t : s.getTopics())
					scienceTopics+=" "+t.getText();
				science++;
			}
			else if(s.getCategory().equalsIgnoreCase("cultura")){
				for(Topic t : s.getTopics())
					cultureTopics+=" "+t.getText();
				culture++;
			}
			else if(s.getCategory().equalsIgnoreCase("opinião")){
				for(Topic t : s.getTopics())
					opinionTopics+=" "+t.getText();
				opinion++;
			}
			total++;
		}

		double politicsProb=0, economicsProb=0, nationalProb=0, internationalProb=0;
		double societyProb=0, sportsProb=0, scienceProb=0, cultureProb=0, opinionProb=0;

		if(politics>0){
			politicsProb=politics/(total*1.0);
		}
		if(economics>0){
			economicsProb=economics/(total*1.0);
		}
		if(national>0){

			nationalProb=national/(total*1.0);
		}
		if(international>0){
			internationalProb=international/(total*1.0);
		}
		if(society>0){
			societyProb=society/(total*1.0);
		}
		if(sports>0){
			sportsProb=sports/(total*1.0);
		}
		if(science>0){
			scienceProb=science/(total*1.0);
		}
		if(culture>0){
			cultureProb=culture/(total*1.0);
		}
		if(opinion>0){
			opinionProb=opinion/(total*1.0);
		}

		int politicsToGet=(int) Math.round(15.0*politicsProb);
		int economicsToGet=(int) Math.round(15.0*economicsProb);
		int nationalToGet=(int) Math.round(15.0*nationalProb);
		int internationalToGet=(int) Math.round(15.0*internationalProb);
		int societyToGet=(int) Math.round(15.0*societyProb);
		int sportsToGet=(int) Math.round(15.0*sportsProb);
		int scienceToGet=(int) Math.round(15.0*scienceProb);
		int cultureToGet=(int) Math.round(15.0*cultureProb);
		int opinionToGet=(int) Math.round(15.0*opinionProb);

		System.out.println("I'll get "+politicsToGet+" politics news");
		System.out.println("I'll get "+economicsToGet+" economics news");
		System.out.println("I'll get "+nationalToGet+" national news");
		System.out.println("I'll get "+internationalToGet+" international news");
		System.out.println("I'll get "+societyToGet+" society news");
		System.out.println("I'll get "+sportsToGet+" sports news");
		System.out.println("I'll get "+scienceToGet+" science news");
		System.out.println("I'll get "+cultureToGet+" culture news");
		System.out.println("I'll get "+opinionToGet+" opinion news");

		List<Message> suggestions = new ArrayList<Message>();
		List<Message> listAux = new ArrayList<Message>();
		LuceneManager lm = new LuceneManager();
		Random randomGenerator = new Random();

		List<Integer> passed = new ArrayList<Integer>();

		/*Politics news*/
		if(politicsToGet>0){
			listAux=lm.luceneSearchSuggestions(politicsTopics, "Política");
			//System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<politicsToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO POLITICA");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("POLITICA SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*Economics news*/
		if(economicsToGet>0){
			listAux=lm.luceneSearchSuggestions(economicsTopics, "Economia");
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<economicsToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO ECONOMIA");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("ECONOMIA SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*national news*/
		if(nationalToGet>0){
			listAux=lm.luceneSearchSuggestions(nationalTopics, "Nacional");
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<nationalToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO NACIONAL");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("NACIONAL SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*international news*/
		if(internationalToGet>0){
			listAux=lm.luceneSearchSuggestions(internationalTopics, "Internacional");
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<internationalToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO INTERNACIONAL");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("INTERNACIONAL SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*society news*/
		if(societyToGet>0){
			listAux=lm.luceneSearchSuggestions(societyTopics, "Sociedade");	
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<societyToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO SOCIEDADE");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("SOCIEDADE SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*culture news*/
		if(cultureToGet>0){
			listAux=lm.luceneSearchSuggestions(cultureTopics, "Cultura");
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<cultureToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO CULTURA");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("CULTURA SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*science news*/
		if(scienceToGet>0){
			listAux=lm.luceneSearchSuggestions(scienceTopics, "Ciência");
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<scienceToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO CIENCIA");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("CIÊNCIA SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*sports news*/
		if(sportsToGet>0){
			listAux=lm.luceneSearchSuggestions(sportsTopics, "Desporto");
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<sportsToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO DESPORTO");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("DESPORTO SEM RESULTS");
			}
		}

		passed = new ArrayList<Integer>();

		/*opinion news*/
		if(opinionToGet>0){
			listAux=lm.luceneSearchSuggestions(opinionTopics, "Opinião");
			System.out.println("LIST SIZE: "+listAux.size());
			if(listAux.size()>0){
				for(int i=0; i<opinionToGet; i++){
					int randomInt = randomGenerator.nextInt(listAux.size());
					boolean verify = true;
					for(Integer j : passed){
						if(j==randomInt){
							verify=false;
							System.out.println("REPETIDO OPINIAO");
							break;
						}
					}
					if(verify){
						suggestions.add(listAux.get(randomInt));
						passed.add(randomInt);
					}
				}
			}
			else{
				System.out.println("OPINIÃO SEM RESULTS");
			}
		}

		return suggestions;
	}





	/**
	------------------------------
	REQUESTS ON THE FLY 
	------------------------------
	 **/

	public static Result autocomplete() {
		DynamicForm data = Form.form().bindFromRequest();
		String search = data.get("search");
		if(!search.equals("")){
			//LuceneManager lm = new LuceneManager();
			//List<String> list = lm.luceneSearch(search);
		}
		List<String> response = new ArrayList<String>();
		response.add(search);
		response.add(search);
		response.add(search);
		return ok(Json.toJson(response));
	}

	/*login facebook*/
	public static Result loginFacebook(){
		DynamicForm data = Form.form().bindFromRequest();
		String search = data.get("test");

		System.out.println("register method: "+search);
		return ok(Json.toJson("teste"));
	}
	public static Result login() throws Exception{

		DynamicForm data = Form.form().bindFromRequest();
		String email = data.get("email");
		String password = data.get("password");
		System.out.println("email: "+email);
		User u = User.find.where().eq("email",email).findUnique();
		if(u!=null){
			if(utils.Password.check(password,u.getPassword())){
				System.out.println("passwords match");
				session("email", u.getEmail());
				session("name", u.getName());
				return ok(Json.toJson("ls"));
			}else{
				System.out.println("passwords don't match");
				return ok(Json.toJson("wp"));
			}
		}
		return ok(Json.toJson("ude"));
	}

	/*register application user*/
	public static Result registerApp(){
		DynamicForm data = Form.form().bindFromRequest();
		String name = data.get("name");
		String email = data.get("email");
		if(name.equals("") || name.length()<4 || name.length()>25){
			return ok(Json.toJson("in"));
		}

		User u = User.find.where().eq("email",email).findUnique();
		if(u!=null){
			return ok(Json.toJson("uae"));
		}
		String birthday = data.get("dpYears");
		String password = data.get("password");
		String retypePassword = data.get("retype");
		System.out.println(name+" "+email);
		String response = "";
		if(!password.equals(retypePassword)){
			//password don't match
			return ok(Json.toJson("pdm"));
		}
		if(password.length()<6){
			return ok(Json.toJson("pts"));
		}

		System.out.println("date: "+birthday);
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
		DateTime date = formatter.parseDateTime(birthday);
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		Boolean valid = email.matches(EMAIL_REGEX);
		System.out.println("e-mail: "+email+" :Valid = " + valid);
		if(!valid){
			return ok(Json.toJson("ie"));
		}

		User userToSave = null;
		try {
			userToSave = new User(name, date, email, Password.getSaltedHash(password));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		userToSave.save();
		return ok(Json.toJson("created"));

	}

	public static Result logout(){
		session().clear();
		return ok(index.render("Início",null,null, null));
	}


	//Category Search

	private static List<Message> pagination(List<Message> allMessages,int page){
		List<Message> results = new ArrayList<Message>();
		int initPage = page*4;
		int endPage = (page+1)*4;
		if(allMessages.size()<initPage){
			return results;
		}else{
			for(int i = initPage; i<endPage;i++ ){
				try{
					results.add(allMessages.get(i));
				}catch(Exception e){
					System.out.println("no more articles");
					break;
				}
			}
		}
		return results;
	}

	public static Result politicsLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Política");
		return ok(search_cat.render(pagination(list, page),"politicslastday",page));
	}
	public static Result politicsAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Política");
		return ok(search_cat.render(pagination(list, page),"politicsall",page));
	}
	public static Result nationalLastDay(int page){
		System.out.println("nationalLastDay method");
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Nacional");
		return ok(search_cat.render(pagination(list, page),"nationallastday",page));
	}
	public static Result nationalAll(int page){
		System.out.println("nationalAll method");
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Nacional");
		return ok(search_cat.render(pagination(list, page),"nationalall",page));
	}
	public static Result internationalLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Internacional");
		return ok(search_cat.render(pagination(list, page),"internationallastday",page));
	}
	public static Result internationalAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Internacional");
		return ok(search_cat.render(pagination(list, page),"internationalall",page));
	}
	public static Result economicsLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Economia");
		return ok(search_cat.render(pagination(list, page),"economicslastday",page));
	}
	public static Result economicsAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Economia");
		return ok(search_cat.render(pagination(list, page),"economicsall",page));
	}
	public static Result cultureLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Cultura");
		return ok(search_cat.render(pagination(list, page),"culturelastday",page));
	}
	public static Result cultureAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Cultura");
		return ok(search_cat.render(pagination(list, page),"culturesall",page));
	}
	public static Result scienceLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Ciência");
		return ok(search_cat.render(pagination(list, page),"sciencelastday",page));
	}
	public static Result scienceAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Ciência");
		return ok(search_cat.render(pagination(list, page),"scienceall",page));
	}
	public static Result societyLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Sociedade");
		return ok(search_cat.render(pagination(list, page),"societylastday",page));
	}
	public static Result societyAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Sociedade");
		return ok(search_cat.render(pagination(list, page),"societyall",page));
	}

	public static Result opinionLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Opinião");
		return ok(search_cat.render(pagination(list, page),"opinionlastday",page));
	}
	public static Result opinionAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Opinião");
		return ok(search_cat.render(pagination(list, page),"opinionall",page));
	}

	public static Result sportsLastDay(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchLastDay("Desporto");
		return ok(search_cat.render(pagination(list, page),"sportslastday",page));
	}
	public static Result sportsAll(int page){
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchByCategory("Desporto");
		return ok(search_cat.render(pagination(list, page),"sportsall",page));
	}

	public static Result latestNews(int page) {
		try{
		LuceneManager lm = new LuceneManager();
		List<Message> list = null;
		list = lm.luceneSearchTopLastDay();

		List<Message> inOrder = new ArrayList<Message>();
		List<Message> toReturn = new ArrayList<Message>(); 
		for(Message m : list){
			int i = 0;
			for(Message msg : inOrder){
				if(m.getViews()<msg.getViews()){
					i++;
				}
				else{
					break;
				}
			}
			inOrder.add(i, m);
		}

		for (int i = 0; i < inOrder.size(); i++) {
			if(i==16){
				break;
			}
			else{
				toReturn.add(inOrder.get(i));
			}
		}
		//System.out.println("return sizE:"+toReturn.size());
		return ok(search_cat.render(pagination(toReturn, page),"topday",page));
		}catch(Exception e){
			return notFound(errorpage.render(" está a tentar aceder a uma página inexistente."));
		}
	}


	//******************************************************************//

}

