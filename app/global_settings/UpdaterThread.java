package global_settings;

import utils.DiarioNoticias;
import utils.Expresso;
import lucene.LuceneManager;



public class UpdaterThread extends Thread {

	
	public UpdaterThread() {
		System.out.println("Starting thread db updater...");
		start();
	}
	public void run() {
		while(!this.interrupted()){
			try {
				System.out.println("INFO: Updating DB...");
				Expresso.index();
				DiarioNoticias.index();
				System.out.println("INFO: Done updating db...");
				System.out.println("INFO: Indexing with Lucene...");
				LuceneManager lm = new LuceneManager();
				lm.indexArticles();
				System.out.println("INFO: Done indexing with Lucene...");
				this.sleep(7200000);
			} catch (Exception e) {
				System.out.println("ERROR: RESTARTING SCRAPING AND INDEXING...");
			}
		}
	}
}
