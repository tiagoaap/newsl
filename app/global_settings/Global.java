package global_settings;


import play.Application;
import play.GlobalSettings;
import play.Logger;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.InstanceList;


public class Global extends GlobalSettings {

	
	public static ParallelTopicModel model = new ParallelTopicModel(100, 1.0, 0.01);
	public static InstanceList instances;
	
	@Override
	public void onStart(Application app) {
		System.out.println("Starting...");
		//Thread thread = new UpdaterThread();
	}  

	@Override
	public void onStop(Application app) {
		Logger.info("Application shutdown...");
	}

}
