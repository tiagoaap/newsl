package utils;

import org.joda.time.DateTime;

public class DateMaker {

	
	public DateMaker(){
	}
	
	public DateTime makeMyDate(String date){
		
		String aux[]=date.split(" de ");
		DateTime realDate = null;
		
		if(aux[1].equalsIgnoreCase("janeiro")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 1, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("fevereiro")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 2, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("março")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 3, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("abril")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 4, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("maio")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 5, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("junho")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 6, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("julho")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 7, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("agosto")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 8, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("setembro")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 9, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("outubro")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 10, Integer.parseInt(aux[0]), 0,0);
		}
		else if(aux[1].equalsIgnoreCase("novembro")){
			realDate = new DateTime(Integer.parseInt(aux[2]), 11, Integer.parseInt(aux[0]), 0,0);
		}
		else{
			realDate = new DateTime(Integer.parseInt(aux[2]), 12, Integer.parseInt(aux[0]), 0,0);
		}

		return realDate;
	}
	
	
}
