package utils;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;

public class POSTaggingManager {



	public ArrayList<String> makeTagging(String article) throws IOException{

		POSModel model = new POSModelLoader().load(new File("pt-pos-maxent.bin"));
		POSTaggerME tagger = new POSTaggerME(model);
		ObjectStream<String> lineStream = new PlainTextByLineStream(new StringReader(article));
		String line;
		String[] tags = null;

		POSSample sample = null;
		while ((line = lineStream.read()) != null) {
			String whitespaceTokenizerLine[] = WhitespaceTokenizer.INSTANCE.tokenize(line);
			tags = tagger.tag(whitespaceTokenizerLine);
			sample = new POSSample(whitespaceTokenizerLine, tags);

			//System.out.println(sample.toString());
		}

		String []entities = sample.getSentence();


		ArrayList<String> terms = new ArrayList<String>();

		for(int i = 0; i<tags.length; i++){
			//System.out.println("TAGS["+i+"]: "+tags[i]);
			//System.out.println("ENTITIES["+i+"]: "+entities[i]);
			if(tags[i].equals("n")){
				//System.out.println("Novo nome comum: "+entities[i]);
				terms.add(entities[i]);
			}
			else 
			if(tags[i].equals("prop")){
				String prop="";
				
				while (i<tags.length && tags[i].equals("prop")){
					
					prop+=entities[i].replaceAll("\"", "")+" ";
					if(entities[i].charAt(entities[i].length()-1)==','){
						break;
					}
					i++;
					//para os de das dos etc entre props
					String aux = "";
					if(i<tags.length){
						if((tags[i].equals("prp") || tags[i].equals("v-pcp")) && !entities[i].equalsIgnoreCase("em")){
							aux = entities[i]+" ";
							i++;
						}
					}
					prop+=aux;
				}
				prop=prop.substring(0,prop.length()-1);
				prop=prop.replaceAll("(?!\")\\p{Punct}", "");
				
				//System.out.println("Novo nome próprio: "+prop);
				
				boolean exists=false;
				for(String s : terms){
					if(s.equalsIgnoreCase(prop)) exists = true;
				}
				
				if(!exists) terms.add(prop);
			}
		}
		
		return terms;

		/*if(terms.isEmpty())
			return "";
		
		String finalTags ="";
		finalTags=terms.get(0);
		for(int i = 0; i<terms.size(); i++){
			finalTags+=";"+terms.get(i);
		}

		return finalTags;*/

	}
}