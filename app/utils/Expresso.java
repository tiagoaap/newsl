package utils;

import java.io.IOException;
import java.util.ArrayList;

import models.Article;

import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import play.mvc.Controller;
import play.mvc.Result;

public class Expresso extends Controller {
  
    public static Result index() {
    	menuIterator();
    	System.out.println("INFO: Done Scraping Expresso");
        return ok("Scraping Expresso Done");
    }

	private static void menuIterator() {
		System.out.println("INFO: Starting scraping...");
		System.out.println("INFO: Performing scraping on EXPRESSO...");
		//Get the number of pages per menu
		ArrayList<String> menuUrls =  new ArrayList<String>();
		menuUrls.add("http://expresso.sapo.pt/politica?mid1=ex.menus/475");
		menuUrls.add("http://expresso.sapo.pt/sociedade?mid1=ex.menus/283");
		menuUrls.add("http://expresso.sapo.pt/internacional?mid1=ex.menus/476");
		menuUrls.add("http://expresso.sapo.pt/gen.pl?sid=ex.sections/23413&p=arquivo&mid1=ex.menus/20&m2=270");
		menuUrls.add("http://expresso.sapo.pt/cultura?mid1=ex.menus/477");
		ArrayList<Integer> pagesPerMenu = new ArrayList<Integer>();
		for(String menu:menuUrls){
			Document doc = null;
			try {
				doc = Jsoup.connect(menu).userAgent("Mozilla").timeout(60000).get();
			} catch (IOException e) {
				System.out.println("ERROR: Timeout requesting page");
				e.printStackTrace();
			}
			String nPages = doc.getElementsByClass("quantas").get(0).text().substring(10);
			nPages = nPages.substring(0,nPages.length()-1);
			pagesPerMenu.add(Integer.parseInt(nPages));
			//System.out.println(""+Integer.parseInt(nPages));
		}
		ArrayList<String> menus = new ArrayList<String>();
		menus.add("http://expresso.sapo.pt/politica?");
		menus.add("http://expresso.sapo.pt/sociedade?");
		menus.add("http://expresso.sapo.pt/internacional?");
		menus.add("http://expresso.sapo.pt/gen.pl?sid=ex.sections/23413&p=arquivo&");
		menus.add("http://expresso.sapo.pt/cultura?");
		
		//iterate over menu's
		for(int i = 0; i<5; i++){
			//iterate over menu's pages
			System.out.println("INFO: Searching in a new category...");
			pages:for(int j = 0; j<pagesPerMenu.get(i);j++){
				if(j>20){
					break;
				}
				Document menu = null;
				try {
					String url = menus.get(i)+"page="+(j+1)+"&num=&npages="+pagesPerMenu.get(i);
					//System.out.println("\nINFO: "+url);
					menu = Jsoup.connect(url).userAgent("Mozilla").timeout(60000).get();
				} catch (Exception e) {
					System.out.println("ERROR: Timeout requesting page");
					continue;
				}
				
				//iterate over articles
				Elements art = null;
				if(i==3){
					art = menu.getElementsByClass("listTit");
				}
				else{
					art = menu.getElementsByTag("article");
				}
				int nArticles = art.size();
				//System.out.println("INFO: This page has "+nArticles+" articles");
				
				for(int k = 0 ;k<nArticles;k++){
					String articleUrl = "";
					if(i==3){
						articleUrl = "http://expresso.sapo.pt"+art.get(k).select("a[href]").get(0).attr("href");
					}
					else{
						 articleUrl = "http://expresso.sapo.pt"+art.get(k).getElementsByTag("h1").select("a[href]").get(0).attr("href");
					}
					String imageUrl = "";
					try{
						imageUrl = art.get(k).select("img").first().absUrl("src");
					}catch(Exception e){
						System.out.println("Article without image");
					}
					
					
					//Get this article's information
					Document articlePage = null;
					try {
						articlePage = Jsoup.connect(articleUrl).userAgent("Mozilla").timeout(60000).get();
						String title = "";
						if(i==3){
							title = art.get(k).text();
						}else{
							title = art.get(k).getElementsByTag("h1").text();
						}
						
						String summary = "";
						String author = "";
						String date = "";
						String category = "";
						String body = " ";
						try{
							summary = articlePage.getElementsByTag("summary").get(0).text();
						}catch(Exception e){
						}
						try{
							author = articlePage.select("span.artigoAutor").first().text();
						}catch(Exception e){
						}
						try{
							date = articlePage.getElementsByTag("time").get(0).text();
							
						}catch(Exception e){
						}
						try{
							Element bodyElements = articlePage.select("section#conteudo").first();
							for(Element e:bodyElements.getElementsByTag("p")){
								if(!e.text().isEmpty()){
									body += e.text()+"\n"; 
								}
							}
						}catch(Exception e){
						}
						if(author.isEmpty()){
							author = "Artigo sem autor, Expresso";
						}
						if(summary.isEmpty()){
							summary = null;
						}
						if(date.isEmpty()){
							date = null;
						}
						
						//Category of the article
						if(i==0){
							category = "Política";
						}else if(i==1){
							category = "Sociedade";
						}else if(i==2){
							category = "Internacional";
						}else if(i==3){
							category = "Economia";
						}else if(i==4){
							category = "Cultura";
						}
						
						String [] dateSplitter = date.split(", ");
						date = dateSplitter[1];
						//System.out.println("date:"+date);
						
						DateMaker dmk = new DateMaker();
						DateTime datetime = dmk.makeMyDate(date);
						
						
						//If body is empty - for articles without text(image galleries,videos etc)
						//else if - article already exists
						//else save
						if(body.isEmpty()){
							System.out.println("WARNING: No body in article, discarding!");
							continue;
						}
						else if(Article.find.where().eq("title", title).findUnique()!=null){
							System.out.println("WARNING: Duplicate article, discarding!");
							break pages;
						}
						
						else{
							Article a =  new Article();
							a.setTitle(title);
							a.setURL(articleUrl);
							a.setSummary(summary);
							a.setAuthor(author);
							a.setDate(datetime);
							a.setCategory(category);
							a.setBody(body);
							a.setSource("Expresso");
							if(!imageUrl.equals("")){
								a.setImage(imageUrl);
							}
							a.save();
							System.out.println("INFO: Article ("+title+") saved!");
						}
						
					} catch (Exception e) {
						System.out.println("ERROR: Parsing article, discarding!");
						e.printStackTrace();
						continue;
					}
				}
			}
		}
	}
}
