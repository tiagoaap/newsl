package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import models.Article;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import play.mvc.Controller;
import play.mvc.Result;

public class DiarioNoticias extends Controller {
	static final String URL = "http://www.dn.pt/";

	static List<String> urls = new ArrayList<String>();
	static int nArticles = 0;
	
	
	public static Result index() {
		
		/*Adding themes*/
		getURIs();
		
		System.out.println("INFO: Performing scraping on DN...");
		
		
		for(String url : urls){
			System.out.println("INFO: Searching in a new category...");
			String category = "";
			
			if(url.contains("portugal")){
				category = "Nacional";
			}
			else if(url.contains("globo")){
				category = "Internacional";
			}
			else if(url.contains("economia")){
				category = "Economia";
			}
			else if(url.contains("ciencia")){
				category = "Ciência";
			}
			else if(url.contains("artes")){
				category = "Cultura";
			}
			else if(url.contains("tv")){
				category = "Cultura";
			}
			else if(url.contains("pessoas")){
				category = "Sociedade";
			}
			else if(url.contains("opiniao")){
				category = "Opinião";
			}
			else if(url.contains("politica")){
				category = "Politica";
			}
			else{
				category = "Desporto";
			}
			
			try {
				pages:for(int i=1; i<=15; i++){
					Document doc = null;
					try {
						String request = url+"?page="+i;
						//System.out.println("First request to "+request);
						doc = Jsoup.connect(request).userAgent("Mozilla").timeout(30000).get();
					} catch (IOException e) {
						System.out.println("ERROR: Timeout");
						continue;
						//e.printStackTrace();
					}
					
					Elements elements = doc.select("div.cx-pesq-item");
					
					
					for(Element e : elements){
						
						String date = e.getElementsByClass("res-data").text();
						String finalURL = "";
						
						try {
							if(((e.getElementsByClass("res-tit").select("a[href]").get(0).attr("href")+"&page=-1").replace(" ","")).contains("www.dn.pt/"))
								finalURL = (e.getElementsByClass("res-tit").select("a[href]").get(0).attr("href")+"&page=-1").replace(" ","");
							else
								finalURL = (URL+e.getElementsByClass("res-tit").select("a[href]").get(0).attr("href")+"&page=-1").replace(" ","");
							
							//System.out.println("Second request to "+finalURL);
							doc = Jsoup.connect(finalURL).userAgent("Mozilla").timeout(30000).get();
						} catch (IOException e1) {
							try {
								if(((e.getElementsByClass("res-tit").select("a[href]").get(0).attr("href")+"&page=1").replace(" ","")).contains("www.dn.pt/"))
									finalURL = (e.getElementsByClass("res-tit").select("a[href]").get(0).attr("href")+"&page=1").replace(" ","");
								else
									finalURL = (URL+e.getElementsByClass("res-tit").select("a[href]").get(0).attr("href")+"&page=1").replace(" ","");
								
								//System.out.println("Second request to "+finalURL);
								doc = Jsoup.connect(finalURL).userAgent("Mozilla").timeout(30000).get();
							} catch (IOException e2) {
								e2.printStackTrace();
							}
							e1.printStackTrace();
						}
						
						String title = doc.select("span#NewsTitle").first().text();
						
						
						String summary="";
						try {
							summary = doc.select("p#NewsSummary").first().text();
						} catch (Exception e2) {
							summary = "Artigo sem sumário, Diário de Notícios";
						}
						
						
						String author="";
						try {
							 author = doc.select("span.artigo-autor").first().text();
						} catch (Exception e2) {
							author = "Artigo sem autor, Diário de Notícios";
						}
							
						Elements body = doc.select("div#Article");
						String sections = "";
						
						Elements subBody = body.get(0).getElementsByTag("p");
						
						for(Element s : subBody){
							sections+=s.getElementsByTag("p").text()+"\n";
						}
						
						
						Article a = Article.find.where().eq("title", title).findUnique();
						if(a==null){
							DateMaker dmk = new DateMaker();
							a = new Article(title, summary, author, dmk.makeMyDate(date.replaceAll("\u00a0", " ")), sections, "Diário de Notícias", category, finalURL);
							a.save();
							System.out.println("INFO: Article ("+title+") saved!");
							nArticles++;
						}
						else{
							System.out.println("WARNING: Duplicate article, discarding!");
							break pages;
						}
						

					}
					
				}
			} catch (Exception e) {
				System.out.println("ERROR: DN Scraping failed on something...");
				//e.printStackTrace();
			}
			

			
		}
		System.out.println();
		return ok("Done... "+nArticles+" new articles in your database!");
	}

	public static void getURIs() {

		/* Início news */
		urls.add(URL + "inicio/portugal/vermais.aspx");
		urls.add(URL + "inicio/globo/vermais.aspx");
		urls.add(URL + "inicio/ciencia/vermais.aspx");
		urls.add(URL + "inicio/artes/vermais.aspx");
		urls.add(URL + "inicio/tv/vermais.aspx");
		urls.add(URL + "inicio/opiniao/vermais.aspx?seccao=Opini%e3o+DN");
		urls.add(URL + "desporto/Benfica/vermais.aspx");
		urls.add(URL + "desporto/Porto/vermais.aspx");
		urls.add(URL + "desporto/Sporting/vermais.aspx");
		urls.add(URL + "desporto/FutebolNacional/vermais.aspx");
		urls.add(URL + "desporto/FutebolInternacional/vermais.aspx");
		urls.add(URL + "desporto/OutrasModalidades/vermais.aspx");
		
	}

}
