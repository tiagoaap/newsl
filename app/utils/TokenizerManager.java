package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class TokenizerManager {

	
	public String getTokens(String line){
		
		InputStream modelIn = null;
		TokenizerModel model = null;
		try {
			modelIn = new FileInputStream("pt-token.bin");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			model = new TokenizerModel(modelIn);
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}
		
		Tokenizer tokenizer = new TokenizerME(model);
		String tokens[] = tokenizer.tokenize(line);
		
		String fTokens="";
		fTokens=tokens[0].replaceAll("(?!\")\\p{Punct}", "");;
		
		for(int i = 1; i<tokens.length; i++){
			fTokens+=";"+tokens[i].replaceAll("(?!\")\\p{Punct}", "");
		}
		
		return fTokens;
	}
	
}
