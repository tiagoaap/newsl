package utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
public class WikiEntityManager {
	static final String URL = "http://pt.wikipedia.org/wiki/";

	public static HashMap<String, String[]> wikiextraction(String in) { 
		String []entities = in.split(";");
		ArrayList<String> entitiesProcessed = new ArrayList<String>();
		HashMap<String, String[]> list = new HashMap<String, String[]>();  
		for(String entity : entities){
			boolean found = false;
			for(String processed: entitiesProcessed){
				if(entity.equals(processed)){
					found = true;
					break;
				}
			}
			if(!found){
				try {
					Document doc;
					doc = Jsoup.connect(URL+URLEncoder.encode(entity.replace(" ","_"),"UTF-8")).userAgent("Mozilla").timeout(60000).get();
					Elements allEl = doc.getAllElements();
					for(Element e: allEl){
						if(e.tagName().equals("p")){
							if(e.text().contains("é um dos") || e.text().contains("é uma das") ||e.text().contains("é um") || e.text().contains("é uma") || e.text().contains("foi um") || e.text().contains("foi uma") || e.text().contains("é a") || e.text().contains("é o")){
								Pattern pattern = Pattern.compile("" +
										"(é uma das ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)|" +
										"(é um dos ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)|" +
										"(é um ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)|" +
										"(é uma ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)|" +
										"(foi um ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)|" +
										"(foi uma ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)|" +
										"(é o ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)|" +
										"(é a ([^(\\s|,)]+)((\\se\\s|,\\s)[^\\s|,]+)*)");
								Matcher matcher = pattern.matcher(e.text());
								while (matcher.find()) {
									break;
								}
								try{
									String parseThis = matcher.group().replaceAll("é um dos ", "").replaceAll("é uma das ", "").replaceAll("é um ", "").replaceAll("é uma ", "").replaceAll("foi um ", "").replaceAll("foi uma ", "").replaceAll("é o ", "").replaceAll("é a ", "").replaceAll(" e ","-").replace(" ", "-").replaceAll(",", "");
									String[] terms = parseThis.split("-");
									list.put(entity, terms);
									//System.out.print("->>>>"+entity+": ");
									//for(String s:terms){
										//System.out.print(s+" ");
									//}
									//System.out.print(" URL: "+URL+URLEncoder.encode(entity.replace(" ","_"),"UTF-8"));
									//System.out.println("");
									entitiesProcessed.add(entity);
								}catch(Exception ex){
									
									System.out.println("SERIOUS ERROR IN "+entity+" "+ex.toString());
								}
								break;
							}
						}
					}
				} catch (IOException e) {
					String[] terms = new String[1];
					terms[0] = "noInfo";
					list.put(entity, terms);
					entitiesProcessed.add(entity);
				}	
			}
		}
		return list;
	}

}
