package utils;

import global_settings.Global;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.regex.Pattern;

import models.Article;
import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.FeatureSequence;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.LabelSequence;

public class TopicModelManager {


	public void trainModel(List<Article> list){
		// Begin by importing documents from text to feature sequences
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

		// Pipes: lowercase, tokenize, remove stopwords, map to features
		pipeList.add( new CharSequenceLowercase() );
		pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
		pipeList.add( new TokenSequenceRemoveStopwords(new File("pt.txt"), "UTF-8", false, false, false) );
		pipeList.add( new TokenSequence2FeatureSequence() );

		Global.instances = new InstanceList (new SerialPipes(pipeList));


		for(Article a : list){
			String text = a.getTitle()+".\t"+a.getSummary()+"\t"+a.getBody();
			Global.instances.addThruPipe(new Instance(text, null, null, null)); // data, label, name fields
		}


		Global.model.addInstances(Global.instances);

		// Use two parallel samplers, which each look at one half the corpus and combine
		//  statistics after every iteration.
		Global.model.setNumThreads(2);

		// Run the model for 50 iterations and stop (this is for testing only, 
		//  for real applications, use 1000 to 2000 iterations)
		Global.model.setNumIterations(1500);

		try {
			Global.model.estimate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public ArrayList<String> getTopics(String article, int id) throws IOException{
		
		//System.out.println("<=============ARTICLE=============>\n"+article+"\n<=============END=============>");
		
		
		InstanceList testing = new InstanceList (Global.instances.getPipe());
		testing.addThruPipe(new Instance(article, null, "test instance", null));

		TopicInferencer inferencer = Global.model.getInferencer();
		double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);

		
		Alphabet dataAlphabet = Global.instances.getDataAlphabet();
		ArrayList<TreeSet<IDSorter>> topicSortedWords = Global.model.getSortedWords();
		
		
		ArrayList<String> fTopics = new ArrayList<String>();
		
		for(int i = 0; i<testProbabilities.length; i++){
			//System.out.println("PROB: "+testProbabilities[i]);
			if(testProbabilities[i]>=0.2){
				System.out.println("PROB: "+testProbabilities[i]);
				Iterator<IDSorter> iterator = topicSortedWords.get(i).iterator();
				int rank = 0;
				String tpcs="";
				while (iterator.hasNext() && rank < 5) {
					IDSorter idCountPair = iterator.next();
					tpcs+=dataAlphabet.lookupObject(idCountPair.getID())+" ";
					rank++;
				}
				
				System.out.println("[id:"+id+"]: NEW TOPIC - "+tpcs.substring(0, tpcs.length()-1));
				fTopics.add(tpcs.substring(0, tpcs.length()-1));
			}
		}
		return fTopics;
	}
}