package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Message implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Article article;
	int count;
	String []tokens;
	String snipets;
	int views;
	
	List<Topic> topics = new ArrayList<Topic>();
	List<Entidade> entities = new ArrayList<Entidade>();
	
	public Message(Article article, int count, String[] tokens) {
		super();
		this.article = article;
		this.count = count;
		this.tokens = tokens;
	}
	
	public Message(Article article, int count, String[] tokens, String snipets) {
		super();
		this.article = article;
		this.count = count;
		this.tokens = tokens;
		this.snipets = snipets;
	}
	
	public Message(Article article, int count, String[] tokens, String snipets, int views) {
		super();
		this.article = article;
		this.count = count;
		this.tokens = tokens;
		this.snipets = snipets;
		this.views=views;
	}
	
	public Message(Article article, int count, String[] tokens, String snipets, int views, List<Topic> topics, List<Entidade> entities) {
		super();
		this.article = article;
		this.count = count;
		this.tokens = tokens;
		this.snipets = snipets;
		this.views=views;
		this.topics=topics;
		this.entities=entities;
	}

	public int getViews() {
		return views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public String getSnipets() {
		return snipets;
	}

	public void setSnipets(String snipets) {
		this.snipets = snipets;
	}

	public Article getArticle() {
		return article;
	}
	
	public void setArticle(Article article) {
		this.article = article;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public String[] getTokens() {
		return tokens;
	}
	
	public void setTokens(String[] tokens) {
		this.tokens = tokens;
	}
	
	public String getTokensStr(){
		String res = "";
		for(String s:tokens){
			res+=s+" ";
		}
		return res;
	}
	

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	public List<Entidade> getEntities() {
		return entities;
	}

	public void setEntities(List<Entidade> entities) {
		this.entities = entities;
	}

	@Override
	public String toString() {
		return "Message [article=" + article + ", count=" + count + ", tokens="
				+ Arrays.toString(tokens) + ", snipets=" + snipets + ", views="
				+ views + "]";
	}
}
