package models;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.DateTime;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;


@Entity
@Table(name = "user")
public class User extends Model{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_id")
	public int id;
	
	@Required
	@Column(name="facebookid", columnDefinition="TEXT")
	private String facebookid= "";
	
	@Required
	@Column(name="name", columnDefinition="TEXT")
	private String name = "";
	
	@Required
	private DateTime birthday;
	
	@Required
	@Column(name="email", columnDefinition="TEXT")
	private String email = "";
	
	@Required
	@Column(name="password", columnDefinition="TEXT")
	private String password = "";
	
	@OneToMany(cascade=CascadeType.ALL)
	public List<Suggestion> suggestions = new ArrayList<Suggestion>();
	

	
	public User(String name, DateTime birthday, String email, String password) {
		super();
		this.name = name;
		this.birthday = birthday;
		this.email = email;
		this.password = password;
	}
	
	public User(String name, DateTime birthday, String email, String password, String facebookid) {
		super();
		this.name = name;
		this.birthday = birthday;
		this.email = email;
		this.password = password;
		this.facebookid=facebookid;
	}
	
	
	public List<Suggestion> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(List<Suggestion> preferences) {
		this.suggestions = preferences;
	}
	
	public void addSuggestions(Suggestion suggestion) {
		this.suggestions.add(suggestion);
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFacebookid() {
		return facebookid;
	}

	public void setFacebookid(String facebookid) {
		this.facebookid = facebookid;
	}

	public DateTime getBirthday() {
		return birthday;
	}

	public void setBirthday(DateTime birthday) {
		this.birthday = birthday;
	}

	
	public static Finder<Integer, User> findById = new Finder<Integer, User>(Integer.class, User.class);
	public static Finder<String, User> find = new Finder<String, User>(String.class, User.class);
}
