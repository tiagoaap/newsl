package models;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.DateTime;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name = "article")
public class Article extends Model{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int id;

	@Required
	@Column(name="title", columnDefinition="TEXT")
	private String title = "";

	@Column(name="summary", columnDefinition="TEXT")
	private String summary = "";

	@Required
	@Column(name="author", columnDefinition="TEXT")
	private String author = "";

	@Required
	private DateTime date = null;

	@Required
	@Column(name="body", columnDefinition="TEXT")
	private String body = "";

	@Required
	@Column(name="source", columnDefinition="TEXT")
	private String source = "";

	@Required
	@Column(name="category", columnDefinition="TEXT")
	private String category = "";

	@Required
	@Column(name="url", columnDefinition="TEXT")
	private String URL = "";

	@Required
	@Column(name="indexed")
	private boolean indexed;
	
	@Required
	@Column(name="image")
	private String image;
	
	@OneToMany(cascade=CascadeType.ALL)
	public List<Comment> comments = new ArrayList<Comment>();

	public Article(){
		this.indexed = false;
	}

	public Article(String title, String summary, String author, DateTime date, String body, String source, String category, String URL) {
		super();
		this.title = title;
		this.summary = summary;
		this.author = author;
		this.date = date;
		this.body = body;
		this.source = source;
		this.category = category;
		this.URL = URL;
		this.indexed = false;
	}

	public Article(int id, String title, String summary, String author, DateTime date, String body, String source, String category, String URL) {
		super();
		this.id=id;
		this.title = title;
		this.summary = summary;
		this.author = author;
		this.date = date;
		this.body = body;
		this.source = source;
		this.category = category;
		this.URL = URL;
		this.indexed = false;
	}
	
	public Article(int id, String title, String summary, String author, DateTime date, String body, String source, String category, String URL,String image) {
		super();
		this.id=id;
		this.title = title;
		this.summary = summary;
		this.author = author;
		this.date = date;
		this.body = body;
		this.source = source;
		this.category = category;
		this.URL = URL;
		this.image = image;
		this.indexed = false;
	}
	
	public void addComment(Comment c) {
		this.comments.add(c);
	}
	
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}


	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public String getFormattedDate(){
		String date = "";
		
		date += pad(this.date.getDayOfMonth()+1)+"-";
		if(date.equals("32-")) date = "31-";
		date += pad(this.date.getMonthOfYear())+"-";
		date += this.date.getYear();
		return date;
	}
	public static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
	public static Finder<String, Article> find = new Finder<String, Article>(
			String.class, Article.class);
	public static Finder<Integer, Article> findById = new Finder<Integer, Article>(
			Integer.class, Article.class);

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", URL=" + URL + "]";
	}
}
