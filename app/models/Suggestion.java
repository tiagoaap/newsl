package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "suggestion")
public class Suggestion extends Model{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int id;
	
	@Required
	private int articleId;
	
	@Required
	private int countViews;
	
	@Required
	private String category;
	
	@OneToMany(cascade=CascadeType.ALL)
	public List<Topic> topics = new ArrayList<Topic>();
	
	@OneToMany(cascade=CascadeType.ALL)
	public List<Entidade> entities = new ArrayList<Entidade>();

	public Suggestion(int articleId, String category, List<Topic> topics, List<Entidade> entities) {
		super();
		this.articleId = articleId;
		this.category = category;
		this.topics = topics;
		this.entities = entities;
		this.countViews=1;
	}
	
	public int getCountViews() {
		return countViews;
	}

	public void setCountViews(int countViews) {
		this.countViews = countViews;
	}

	public void updateCountViews() {
		this.countViews++;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	public List<Entidade> getEntities() {
		return entities;
	}

	public void setEntities(List<Entidade> entities) {
		this.entities = entities;
	}

	@Override
	public String toString() {
		return "Favorite [id=" + id + ", articleId=" + articleId
				+ ", category=" + category + ", topics=" + topics
				+ ", entities=" + entities + "]"
				+ ", counts=" + countViews + "]";
	}
	
	public static Finder<Integer, Suggestion> find = new Finder<Integer, Suggestion>(
			Integer.class, Suggestion.class);
	
}
