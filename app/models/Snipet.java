package models;

public class Snipet {
	
	int articleId;
	String text;
	
	public Snipet(int articleId, String text){
		this.articleId=articleId;
		this.text=text;
	}

	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Snipet [articleId=" + articleId + ", text=" + text + "]";
	}
	
}
